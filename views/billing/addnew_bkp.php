<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Bill</h3>
   </div>
   <style type="text/css">
      table th, table td{
      text-align: center;
      }
      table tfoot tr th{
      vertical-align: middle !important;
      }

      
      #client_list{float:left;list-style:none;padding:0;width:100%;position: relative;z-index:999999;}
      #client_list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
      #client_list li:hover{background:#6a7a91;cursor: pointer; color:#fff;}

      #chassis_id{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
   </style>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=billing&task=show"><i class="fa fa-list" aria-hidden="true"></i> Bill List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Fees</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Fees</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php
      unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }


     $bill = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `bill_fare` WHERE `bill_no`='".$result['bill_no']."' AND `id`='".$result['bill_id']."' AND `mobile`='".$result['cust_mobile']."'"));
      
      ?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="col-md-12">
            <div class="col-md-3">
               <label>Class: </label>
               <!-- <input type="text" name="class_id" id="class_id" value="<?php echo $bill['class_id']; ?>" class="form-control" required=""> -->
               <select id="class_id" name="class_id" class="form-control class_id select2" style="width: 100%;" required="">
                  <option value="">Select</option>
                  <?php $fee_type = mysqli_query($conn,"SELECT * FROM `class_master` WHERE `status`= 1 ORDER BY `id` ASC");
                  while($fees = mysqli_fetch_array($fee_type)){ ?>
                  <option value="<?php echo $fees['id']; ?>"><?php echo $fees['name'];  ?></option>
                  <?php } ?>
               </select>
            </div>
            <div class="col-md-3">
               <label>Student: </label>
               <input type="hidden" name="student_name" id="student_name" value="<?php echo $bill['student_name']; ?>" class="form-control" required="">
               <select id="students" name="students" class="form-control class_id select2" style="width: 100%;" required="">
                  <option value="">Select</option>
                  <?php /*$fee_type = mysqli_query($conn,"SELECT * FROM `class_master` WHERE `id`= $_REQUEST[] ORDER BY `id` ASC");
                  while($fees = mysqli_fetch_array($fee_type)){ ?>
                  <option value="<?php echo $fees['id']; ?>"><?php echo $fees['name'];  ?></option>
                  <?php }*/ ?>
               </select>
            </div>
            <div class="col-md-3">
               <label>Session: </label>
               <!-- <input type="text" name="class_id" id="class_id" value="<?php echo $bill['class_id']; ?>" class="form-control" required=""> -->
               <select id="session" name="session" class="form-control class_id select2" style="width: 100%;" required="">
                  <option value="">Select</option>
                  <?php $fr_yr = date('Y')-1;
                  for($i=0; $i<=2; $i++){ 
                     $session = ($fr_yr+$i).'-'.(($fr_yr+1)+$i); ?>
                  <option value="<?php echo $session; ?>" <?php echo $_SESSION['financial_year']==$session?'selected':''; ?>><?php echo $session;  ?></option>
                  <?php } ?>
               </select>
            </div>
            <div class="col-md-3">
               <label>Remark: </label>
               <input type="text" name="remark" id="remark" value="<?php echo $bill['remark']; ?>" class="form-control" required="">
               
            </div>
<!--             <div class="col-md-3 ui-widget"><label>Roll No: </label>
               <input type="text" name="mobile" id="mobile" value="<?php echo $bill['mobile']; ?>" maxlength="10" pattern="[6789][0-9]{9}" class="form-control " required="">
             
               <span id="suggesstion_client-box"></span>
            </div>
            <div class="col-md-3"><label>Student Name: </label>
               <input type="text" name="student_name" id="student_name" value="<?php echo $bill['student_name']; ?>" class="form-control" required="">
            </div>
            <div class="col-md-3"><label>Class: </label>
               <input type="text" name="class_id" id="class_id" value="<?php echo $bill['class_id']; ?>" class="form-control" required="">
            </div>
            <div class="col-md-3"><label> Section:  </label>
               <input type="text" name="section" id="section" value="<?php echo $bill['section']; ?>" class="form-control" required="">
            </div> -->
         </div>
         <div class="clearfix"></div>
         <hr>
         <div class="row col-md-12 table-responsive">
            <div id="error" style="display: none;">
               <div id="myModal" class="modal fade" role="dialog" style="display: block;">
                  <div class="modal-dialog">
                     <center>
                        <div class="alert alert-danger alert-dismissable" style="margin-top: 40%;">
                           <strong>You Can not add Itmes more than 50</strong>
                        </div>
                     </center>
                  </div>
               </div>
            </div>
            <table class="table table-bordered table-striped">
               <thead>
                  <tr>
                     <th width="50">Sl No</th>
                     <th width="350" colspan="2">Fees Type</th>
                     <th width="300">Amount (<i class="fa fa-inr"></i>)</th>
                     <th width="50">Action</th>
                  </tr>
               </thead>
               <tbody id="add_items">
                  <?php if($_REQUEST['id']){ 
                     foreach($results as $result){ 
                     $i++;
                     ?>
                  <tr>
                     <td><strong class="slno"><?php echo $i; ?></strong></td>
                     <td colspan="2">
                     <select name="items[]" class="form-control items select2" style="width: 100%;" required="">
                     <option value="">Select</option>
                     <?php
                      $product = mysqli_query($conn,"SELECT * FROM `fee_type` WHERE `status`= 1");
                      while($items = mysqli_fetch_array($product)){ ?>
                     <option value="<?php echo $items['id']; ?>" <?php echo $result['id']==$items['id']?'selected':''; ?>><?php echo $items['name']; ?></option>
                     <?php } ?>
                     </select>
                     </td>
                     <td><input type="text" value="<?php echo $result['amount']; ?>" name="amount[]" id="amount<?php echo $i; ?>" class="form-control amount t_amount chk_number" readonly=""></td>
                     <td><a href="javascript:;" class="remove" id="remove<?php echo $i; ?>" ><i class="fa fa-minus-circle fa-2x"></i></a></td>
                  </tr>
             <?php } }else{ ?>
                  <tr>
                     <td><strong class="slno">1</strong></td>
                     <td colspan="2">
                        <select id="fees_type" name="fees_type[]" class="form-control items select2" style="width: 100%;" required="">
                           <option value="">Select</option>
                           <?php $fee_type = mysqli_query($conn,"SELECT * FROM `fee_type` WHERE `status`= 1");
                           while($fees = mysqli_fetch_array($fee_type)){ ?>
                           <option value="<?php echo $fees['id']; ?>"><?php echo $fees['name'];  ?></option>
                           <?php } ?>
                        </select>
                     </td>
                     <td><input type="text" value="0" name="amount[]" onkeyup="change_qty(this.value,1);" id="amount1" class="form-control amount t_amount chk_number" required=""></td>
                     <td><a href="javascript:;" class="remove" id="remove1" style="display: none;"><i class="fa fa-minus-circle fa-2x"></i></a></td>
                  </tr>
               <?php } ?>
               </tbody>
               <tfoot>
               	  <tr  valign="middle" align="center" style="border-top:2px solid #bdbdbd; ">
                        <th></th>
                        <th></th>
                        <th style="width: 200px;"> <span style="float: right;padding-top: 5px;">Sub Total(<i class="fa fa-inr"></i>): </span></th>
                        <th ><input type="text" value="<?php echo $bill['total_amount']?$bill['total_amount']:0; ?>" name="total_amt" id="total_amt" class="form-control" readonly=""></th>
                     <th style="text-align: left;">/-
                        </th>
                        <!-- <th style="width: 200px;"> <span style="float: right;padding-top: 5px;">Payment Mode: </span></th>
                        <th >
                          
                           <select style="width: 100%;float: right;" class="form-control select2" name="select_paymode" id="select_paymode " required="">
                              <option value="">Select</option>
                              <?php $sql = mysqli_query($conn,"SELECT * FROM `payment_mode` WHERE `status`=1 ORDER BY `mode` ASC");
                                 while($pay = mysqli_fetch_array($sql)){ ?>
                              <option value="<?php echo $pay['id']; ?>" <?php echo $bill['payment_mode']==$pay['id']?'selected':''; ?>><?php echo $pay['mode']; ?></option>
                              <?php } ?>
                           </select>
                        </th> -->
                    </tr>
                  <tr>
                     <th ></th>
                     <th ><a href="javascript:;" class="btn btn-success add_more" style="float: right;"><i class="fa fa-plus-circle " style="cursor: pointer;"> Add More</i></a></th>
                     <th style="text-align: right;">Payment (<i class="fa fa-inr"></i>):</th>
                     <th><input onkeyup="change_gtotal()" type="text" value="<?php echo $bill['paid_amount']?$bill['paid_amount']:0; ?>" name="paid_amount" id="paid_amount" class="form-control" required=""></th>
                     <th style="text-align: left;">/-</th>
                  </tr>
                  <!-- <tr valign="middle" align="center" >
                     <th colspan="3"></th>
                     <th style="text-align: right;">Discount (%):</th>
                     <th>
                        <select class="form-control" name="select_disc" id="select_disc" required="">
                           <?php $sql = mysqli_query($conn,"SELECT * FROM `bill_discount` WHERE `status`=1");
                              while($disc = mysqli_fetch_array($sql)){ ?>
                           <option value="<?php echo $disc['percent']; ?>" <?php echo $bill['disc_percent']==$disc['percent']?'selected':''; ?>><?php echo $disc['percent']!='Other Amount'?$disc['percent'].'%':$disc['percent']; ?></option>
                           <?php } ?>
                        </select>
                     <th><input type="text" value="<?php echo $bill['total_discount']?$bill['total_discount']:0; ?>" onkeyup="change_disc();" placeholder="Enter Amount" name="total_disc" id="total_disc" class="form-control chk_number" readonly=""></th>
                     </th>
                     <th style="text-align: left;">/-</th>
                  </tr> -->
                  <tr valign="middle" align="center">
                     <th ></th>
                     <!-- <th ></th> -->
                    
                     <!-- <th width="300" style="text-align: right;">Payment Mode</th> -->
                     <th >
                     	<span style="width: 48%;float: left;padding-top: 5px;">Payment Mode: </span>
                        <select style="width: 50%;float: right;" class="form-control select2" name="select_paymode" id="select_paymode " required="">
                           <option value="">Select</option>
                           <?php $sql = mysqli_query($conn,"SELECT * FROM `payment_mode` WHERE `status`=1 ORDER BY `mode` ASC");
                              while($pay = mysqli_fetch_array($sql)){ ?>
                           <option value="<?php echo $pay['id']; ?>" <?php echo $bill['payment_mode']==$pay['id']?'selected':''; ?>><?php echo $pay['mode']; ?></option>
                           <?php } ?>
                        </select>
                     </th> 
                     <!-- <th ><input type="text" class="form-control" name="card_number" id="card_number" style="display: none;" pattern="[0-9]+" maxlength="4" placeholder="Last 4 Digit(XXXX)" value="<?php echo $bill['card_number']; ?>"></th> -->
                     <th style="text-align: right;">Grand Total (<i class="fa fa-inr"></i>):</th>
                     <th><input type="text" name="grand_total" value="<?php echo $bill['grand_total']?$bill['grand_total']:0; ?>" id="grand_total" class="form-control" readonly=""></th>
                     <th style="text-align: left;">/-</th>
                  </tr>
                  <!-- <tr  valign="middle" align="center" >
                        <th></th>
                        <th></th>
                        <th style="width: 200px;"> <span style="float: right;padding-top: 5px;">Payment Mode: </span></th>
                        <th >
                          
                           <select style="width: 100%;float: right;" class="form-control select2" name="select_paymode" id="select_paymode " required="">
                              <option value="">Select</option>
                              <?php $sql = mysqli_query($conn,"SELECT * FROM `payment_mode` WHERE `status`=1 ORDER BY `mode` ASC");
                                 while($pay = mysqli_fetch_array($sql)){ ?>
                              <option value="<?php echo $pay['id']; ?>" <?php echo $bill['payment_mode']==$pay['id']?'selected':''; ?>><?php echo $pay['mode']; ?></option>
                              <?php } ?>
                           </select>
                        </th>
                    </tr> -->
               </tfoot>
            </table>
            <div>
               <center>
                  
                  <?php if($_REQUEST['id']){ ?>
                  <input type="submit" id="submitBtn" class="btn btn-primary bulu" name="submit" value="Update Bill"><?php }else{ ?>
                  <input type="submit" id="submitBtn" class="btn btn-primary bulu" name="submit" value="Submit Bill"><?php } ?>
                  <a href="javascript:;" id="resetBtn" class="btn btn-primary red">Reset</a>
                  <input type="hidden" name="control" value="billing">
                  <input type="hidden" name="task" value="save">
                  <input type="hidden" name="view" value="show">
                  <!-- <input type="hidden" name="total_item"  id="total_item" value="<?php echo $bill['total_item']?$bill['total_item']:'1'; ?>"> -->
                  <!-- <input type="hidden" name="total_qty"  id="total_qty" value="<?php echo $bill['total_qty']?$bill['total_qty']:'1'; ?>"> -->
                  <input type="hidden" name="id" value="<?php echo $bill['id']; ?>">
                  <input type="hidden" name="bill_id" value="<?php echo $bill['bill_no']; ?>">
                  <input type="hidden" name="bill_date" value="<?php echo $bill['date_created']; ?>">
               </center>
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<!-- <script src="template/sms/plugins/select2/select2.full.min.js"></script> -->
<script type="text/javascript">
/*//Masking Date
$("input[name='dob']").on("keyup", function(){
      $("input[name='number']").val(destroyMask(this.value));
    this.value = createMask($("input[name='number']").val());
})

function createMask(string){
  console.log(string)
   return string.replace(/(\d{2})(\d{2})(\d{4})/,"$1-$2-$3");
}

function destroyMask(string){
  console.log(string)
   return string.replace(/\D/g,'').substring(0, 8);
}
*/


$('#submitBtn').click(function(){
$(this).delay(600).hide(1);
});
$('#resetBtn').click(function(){
$('#submitBtn').show(1);
});


   $("#mobile").keyup(function(){
      if($(this).val().length>=2){
      $.ajax({
      type: "POST",
      url: "script/student_mobile.php",
      data:'num='+$(this).val(),

      beforeSend: function(){
         $("#mobile").css("background","url(LoaderIcon.gif) right 5px bottom 5px no-repeat rgb(255, 255, 255)");
      },
      success: function(data){
         $("#suggesstion_client-box").show();
         $("#suggesstion_client-box").html(data);
         $("#mobile").css("background","#FFF");
      }
      });
   }else if($(this).val().length<2){
         $("#suggesstion_client-box").hide();
      }
   });
   
   function selectMobile(val,objid) {
$("#mobile").val(val);
$("#suggesstion_client-box").hide();

   $.get("script/get_student_detail.php?num="+val, function(data){
    // alert(data);
    value = data.split("#");
    $('#student_name').val(value[0]);
    $('#class_id').val(value[1]);
      $('#section').val(value[2]);
   });

}
 
   
   $('form').submit(function(){
   $(this).children('input[type=submit]').prop('disabled', true);
   });
   
   $('.chk_number').keypress(function(evt){
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : event.keyCode;
   if (charCode > 31
   // if (charCode != 46 && charCode > 31
   && (charCode < 48 || charCode > 57))
   return false;
   
   return true;
   });
   
   /*=================Add Boxes=================*/
   $('.add_more').click(function(){
   n = $('tbody#add_items tr').length +1;
   // alert(n);
   if(n>50){
   show_error();
   // alert("You Can not add Itmes more than 50");
   return 0;
   }else{
   var html_box = '<tr><td><strong class="slno">'+n+'</strong></td><td colspan="2"><select name="fees_type[]"  class="form-control items select12" id="fees_type'+n+'" style="width: 100%;" required=""><option value="">Select</option><?php $product = mysqli_query($conn,"SELECT * FROM `fee_type` WHERE `status`=1"); while($items = mysqli_fetch_array($product)){ ?><option value="<?php echo $items["id"]; ?>"><?php echo $items['name'];  ?></option><?php } ?></select></td><td><input type="text" value="0" name="amount[]" id="amount'+n+'"  onkeyup="change_qty(this.value,'+n+');" class="form-control amount t_amount chk_number" value="" required=""></td><td><a href="javascript:;" class="remove" id="remove'+n+'"><i class="fa fa-minus-circle fa-2x"></i></a></td></tr>';
   $('#add_items').append(html_box);
   // $('#total_item').val(n);
   
   }
   $(".select12").select2();
   $('.remove').click(function(){
   $(this).closest('tr').remove();
   $('#total_item').val(($('tbody#add_items tr').length));
   $('td strong.slno').text(function (i) { return i + 1; });
   /*==========================================*/
   $('td a.remove').each(function(a){
    $(this).attr('id','remove'+parseInt(a+1));
   });
   /*==========================================*/
   $('td select.items').each(function(a){
    $(this).attr('id','fees_type'+parseInt(a+1));
    // $(this).attr('onchange','change_item(this.value,'+parseInt(a+1)+')');
   });
   /*==========================================*/
   $('td input.amount').each(function(e){
    $(this).attr('id','amount'+parseInt(e+1));
   });
   /*==========================================*/
   change_qty(n, n);
   change_gtotal();
   });

   $('.chk_number').keypress(function(evt){
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : event.keyCode;
   if (charCode > 31
   // if (charCode != 46 && charCode > 31
   && (charCode < 48 || charCode > 57))
   return false;
   
   return true;
   });
   });
   
   function show_error(){
   $('#error').show();
   $('#myModal').addClass('in'); 
   $("#error").delay(2000).slideUp(200, function() {
   $(this).alert('close'); 
   });
   }
   /*===========================================*/
   
   function change_item(val, sl){
/*   $.get("script/billing_items.php?pid="+val, function(data){
    // alert(data);
    value = data.split("#");
    $('#qty'+sl).val(value[0]);
    $('#rate'+sl).val(parseFloat(value[1]).toFixed(2));
    // $('#disc_old'+sl).val(value[2]);
    $('#discount'+sl).val(parseFloat(value[2]).toFixed(2));   
    $('#item_name'+sl).val(value[3].trim()); 
    // change_qty(val,sl);
       
  change_gtotal();
   });*/
   // amt = $('#').val();
   }
$('#class_id').change(function(){
   class_id = $(this).val();
   $.get("script/student_list.php?class_id="+class_id, function(data){
      $('#students').html(data);
   })
});

function change_qty(val, sl){

	   var amt_total = 0;
   $('.t_amount').each(function () {
      amt_total += parseFloat(this.value) || 0;
   $('#total_amt').val(amt_total.toFixed(2));
});
    change_gtotal();
   }

   
   
   function change_gtotal(){
   total_amt = parseFloat($('#total_amt').val());
   paid_amount = parseFloat($('#paid_amount').val());

     final_amt = total_amt-paid_amount;
   $('#grand_total').val(parseFloat(final_amt).toFixed(0));
   
   }
   
   
   
  $(".select2").select2();   
   
<?php if($_REQUEST['id']){ ?>
   n = $('tbody#add_items tr').length +1;
   // $(".select2").select2();
   $('.remove').click(function(){
   $(this).closest('tr').remove();
   $('#total_item').val(($('tbody#add_items tr').length));
   $('td strong.slno').text(function (i) { return i + 1; });  
    /*==========================================*/
   $('td a.remove').each(function(a){
    $(this).attr('id','remove'+parseInt(a+1));
   });
   /*==========================================*/
   $('td select.items').each(function(a){
    $(this).attr('id','fee_type'+parseInt(a+1));
    // $(this).attr('onchange','change_item(this.value,'+parseInt(a+1)+')');
   });
   $('td input.amount').each(function(e){
    $(this).attr('id','amount'+parseInt(e+1));
   });
   /*==========================================*/
   $('td a.remove').each(function(e){
    $(this).attr('id','remove'+parseInt(e+1));
   });
   /*==========================================*/
 change_qty(n, n);
 change_gtotal(); 
   }); 
   // change_qty(n, n);
<?php } ?>

$('#students').change(function(){
   console.log($('#students:selected').text());
});
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

