<!-- <div class="panel panel-default"> -->
<!--    <div class="col-md-6" style="padding:0px;">
   </div> -->
   
<section class="content-header">
 <h1>
   Dashboard
   <!-- <small>Version 2.0</small> -->
 </h1>
 <ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   <li class="active">Dashboard</li>
 </ol>
</section>
   <?php 
      if($_SESSION['utype']=='Administrator') { ?> 
<!-- <div class="panel panel-default"> -->
<section class="content">

<hr>
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
               <a href="index.php?control=menu&task=show" class="dashboard-panel-link">
                  <span class="info-box-icon"><i class="ion ion-android-options"></i></span>
               </a>
              <div class="info-box-content">
                <span class="info-box-text">Sidebar Menus</span>
                <span class="info-box-number">5,200</span>
                <div class="progress">
                  <!-- <div class="progress-bar" style="width: 50%"></div> -->
                </div>
                <span class="progress-description">
                  <!-- 50% Increase in 30 Days -->
                </span>
              </div><!-- /.info-box-content -->
              <!-- <a href="#" class="small-box-footer dashboard-panel-link">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
         </div>
         <!-- ./col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
               <a href="#" class="dashboard-panel-link">
              <span class="info-box-icon"><i class="ion ion-ios-people-outline"></i></span>
           </a>
              <div class="info-box-content">
                <span class="info-box-text">Total Student</span>
                <span class="info-box-number">92,050</span>
                <div class="progress">
                  <!-- <div class="progress-bar" style="width: 20%"></div> -->
                </div>
                <span class="progress-description">
                  <!-- 20% Increase in 30 Days -->
                </span>
              </div><!-- /.info-box-content -->
              <!-- <a href="#" class="small-box-footer dashboard-panel-link">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
         </div>
         <!-- ./col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
               <a href="#" class="dashboard-panel-link">
              <span class="info-box-icon"><i class="ion ion-social-usd-outline"></i></span>
           </a>
              <div class="info-box-content">
                <span class="info-box-text">Todays's Collection</span>
                <span class="info-box-number">114,381</span>
                <div class="progress">
                  <!-- <div class="progress-bar" style="width: 70%"></div> -->
                </div>
                <span class="progress-description">
                  <!-- 70% Increase in 30 Days -->
                </span>
              </div><!-- /.info-box-content -->
              <!-- <a href="#" class="small-box-footer dashboard-panel-link">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
         </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
               <a href="#" class="dashboard-panel-link">
          <span class="info-box-icon"><i class="ion ion-cash"></i></span>
       </a>
          <div class="info-box-content">
            <span class="info-box-text">Total Collection</span>
            <span class="info-box-number">163,921</span>
            <div class="progress">
              <!-- <div class="progress-bar" style="width: 40%"></div> -->
            </div>
            <span class="progress-description">
              <!-- 40% Increase in 30 Days -->
            </span>
          </div><!-- /.info-box-content -->
          <!-- <a href="#" class="small-box-footer dashboard-panel-link">More info <i class="fa fa-arrow-circle-right"></i></a> -->
         </div>
         </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
               <a href="#" class="dashboard-panel-link">
              <span class="info-box-icon"><i class="ion ion-android-clipboard"></i></span>
           </a>
              <div class="info-box-content">
                <span class="info-box-text">Report</span>
                <span class="info-box-number">5,200</span>
                <div class="progress">
                  <!-- <div class="progress-bar" style="width: 50%"></div> -->
                </div>
                <span class="progress-description">
                  <!-- 50% Increase in 30 Days -->
                </span>
              </div><!-- /.info-box-content -->
              <!-- <a href="#" class="small-box-footer dashboard-panel-link">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
         </div>
         <!-- ./col -->
         <?php /* ?>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
               <a href="#" class="dashboard-panel-link">
              <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
           </a>
              <div class="info-box-content">
                <span class="info-box-text">Mentions</span>
                <span class="info-box-number">92,050</span>
                <div class="progress">
                  <div class="progress-bar" style="width: 20%"></div>
                </div>
                <span class="progress-description">
                  20% Increase in 30 Days
                </span>
              </div><!-- /.info-box-content -->
            </div>
         </div>
         <!-- ./col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
               <a href="#" class="dashboard-panel-link">
              <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>
           </a>
              <div class="info-box-content">
                <span class="info-box-text">Downloads</span>
                <span class="info-box-number">114,381</span>
                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  70% Increase in 30 Days
                </span>
              </div><!-- /.info-box-content -->
              <!-- <a href="#" class="small-box-footer dashboard-panel-link">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            </div>
         </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
               <a href="#" class="dashboard-panel-link">
          <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>
       </a>
          <div class="info-box-content">
            <span class="info-box-text">Direct Messages</span>
            <span class="info-box-number">163,921</span>
            <div class="progress">
              <div class="progress-bar" style="width: 40%"></div>
            </div>
            <span class="progress-description">
              40% Increase in 30 Days
            </span>
          </div><!-- /.info-box-content -->
          <!-- <a href="#" class="small-box-footer dashboard-panel-link">More info <i class="fa fa-arrow-circle-right"></i></a> -->
         </div>
         </div>
         <?php */ ?>
      </div>

      <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Monthly Recap Report</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <div class="btn-group">
                <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </div>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8">
                <p class="text-center">
                  <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                </p>
                <div class="chart">
                  <!-- Sales Chart Canvas -->
                  <canvas id="salesChart" style="height: 180px;"></canvas>
                </div><!-- /.chart-responsive -->
              </div><!-- /.col -->
              <div class="col-md-4">
                <p class="text-center">
                  <strong>Goal Completion</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Add Products to Cart</span>
                  <span class="progress-number"><b>160</b>/200</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                  </div>
                </div><!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Complete Purchase</span>
                  <span class="progress-number"><b>310</b>/400</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                  </div>
                </div><!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Visit Premium Page</span>
                  <span class="progress-number"><b>480</b>/800</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                  </div>
                </div><!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Send Inquiries</span>
                  <span class="progress-number"><b>250</b>/500</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                  </div>
                </div><!-- /.progress-group -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- ./box-body -->
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                  <h5 class="description-header">$35,210.43</h5>
                  <span class="description-text">TOTAL REVENUE</span>
                </div><!-- /.description-block -->
              </div><!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                  <h5 class="description-header">$10,390.90</h5>
                  <span class="description-text">TOTAL COST</span>
                </div><!-- /.description-block -->
              </div><!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                  <h5 class="description-header">$24,813.53</h5>
                  <span class="description-text">TOTAL PROFIT</span>
                </div><!-- /.description-block -->
              </div><!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                  <h5 class="description-header">1200</h5>
                  <span class="description-text">GOAL COMPLETIONS</span>
                </div><!-- /.description-block -->
              </div>
            </div><!-- /.row -->
          </div><!-- /.box-footer -->
        </div><!-- /.box -->
      </div><!-- /.col -->
      </div><!-- /.row -->
</section>
<!-- </div> -->

   <?php } 
      ?>

<!-- ChartJS 1.0.1 -->
<script src="<?php echo $tmp;?>/plugins/chartjs/Chart.min.js"></script>
<script type="text/javascript">
     //-----------------------
  //- MONTHLY SALES CHART -
  //-----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas);

  var salesChartData = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
      {
        label: "Electronics",
        fillColor: "rgb(210, 214, 222)",
        strokeColor: "rgb(210, 214, 222)",
        pointColor: "rgb(210, 214, 222)",
        pointStrokeColor: "#c1c7d1",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgb(220,220,220)",
        data: [65, 59, 80, 81, 56, 55, 40]
      },
      {
        label: "Digital Goods",
        fillColor: "rgba(60,141,188,0.9)",
        strokeColor: "rgba(60,141,188,0.8)",
        pointColor: "#3b8bba",
        pointStrokeColor: "rgba(60,141,188,1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(60,141,188,1)",
        data: [28, 48, 40, 19, 86, 27, 90]
      }
    ]
  };

  var salesChartOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: false,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: false,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);

  //---------------------------
  //- END MONTHLY SALES CHART -
  //---------------------------
</script>

