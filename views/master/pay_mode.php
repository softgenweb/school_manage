<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-8">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Payment Mode</h3>
            <?php foreach($results as $result) { }  ?>
        <!--    <a href="index.php?control=master&task=addnew_paymode" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Payment</a>-->
            
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Payment Mode List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15">
                              <div align="center">S.No</div>
                           </th>
                           <!-- <th><div align="center">Username</div></th>-->
                           <th>
                              <div align="center">Payment Mode</div>
                           </th>
                           <th>
                              <div align="center">Action</div>
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>
                           <!--  <td align="center"><?php echo $result['username'];?></td>   -->                   
                           <td align="center"><?php echo $result['mode'];?></td>

                           <td align="center">
                              <a href="index.php?control=master&task=pay_mode&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
                              <?php
                                 if($result['status']==1){  ?>
                              <a href="index.php?control=master&task=status_paymode&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                              <?php } else { ?>
                              <a href="index.php?control=master&task=status_paymode&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                              <?php } ?>
                              <!--<a href="index.php?control=regional_hierarchy&task=delete&id=<?php echo $result['id']; ?>" title="Delete" onclick="return confirm('Are you sure want to Delete?')"><img src="images/del.png" alt="Delete" title="Delete" /></a>
                                 &nbsp; &nbsp;
                                 <a onclick="return confirm('Are you sure you want to Change Password ?')" href="index.php?control=regional_hierarchy&task=changepassword&id=<?php echo $result['id']; ?>"><img src="images/reload.png" height="15" width="15" alt="Reset" title="Reset" /></a>-->
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   
       <?php foreach($datas as $data) { }  ?>
   <div class="col-xs-4">
   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"> Add/Edit Payment Mode</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
       
                <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" > 
                <div class="box-body">
                <div class="form-group">
                    <label>Name <?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-pencil"></i>
                      </div>
                         <input type="text" value="<?php echo $data['mode']; ?>" id="mode" name="mode"  class="form-control remove_radious"  required="">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div><!-- /.box-body -->
                     
                <div class="box-footer clearfix">
                  <input type="submit" name="submit" class="btn btn-sm btn-info btn-flat pull-left" value="<?php echo $_REQUEST['id']!=''?'Update':'Submit';?>">
                       <input type="hidden" name="control" value="master"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="save_paymode"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $datas[0]['id']; ?>"  />
                  <!--<a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>-->
                </div><!-- /.box-footer -->
               </form>
            </div>
   
      
      <!-- /.box -->
   </div>
   
   
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

