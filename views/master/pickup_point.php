<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-8">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Student Pick-up Point</h3>
            <?php foreach($results as $result) { }  ?>
            <!--<a href="index.php?control=master&task=addnew_fees" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Fee Type</a>-->
                        
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Student Pick-up Point </li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15">
                              <div align="center">S.No</div>
                           </th>
                           <th><div align="center">Pick-up Point</div></th>
                           <th><div align="center">Fare Amount</div></th>
                           <th>
                              <div align="center">Action</div>
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>                   
                           <td align="center"><?php echo $result['name'];?></td>            
                           <td align="center"><?php echo $result['amount'];?></td>
                           <td align="center">
                              <a href="index.php?control=master&task=pickup_point&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
                              <?php
                                 if($result['status']==1){  ?>
                              <a href="index.php?control=master&task=status_pickup_point&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                              <?php } else { ?>
                              <a href="index.php?control=master&task=status_pickup_point&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                              <?php } ?>
                            
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   
        <?php foreach($datas as $data) { }  ?>
   <div class="col-xs-4">
   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">  <?php if($data['id']){echo "Edit";}else{echo "Add New";}?> Pick-up Point</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
       
                <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" > 
                <div class="box-body">
                <div class="form-group">
                    <label>  Student Pick-up Point  <?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-pencil"></i>
                      </div>
                        <input type="text" value="<?php echo $data['name'];?>" id="name" name="name" class="form-control remove_radious"  required="">
                    
                    </div><!-- /.input group -->
                  </div>
                  
                <div class="form-group">
                    <label>Fare Amount  <?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-inr"></i>
                      </div>
                        <input type="text" value="<?php echo $data['amount'];?>" id="amount" name="amount" class="form-control remove_radious"  required="">
                    
                    </div><!-- /.input group -->
                  </div>  
                </div><!-- /.box-body -->
                     
                <div class="box-footer clearfix">
                  <input type="submit" name="submit" class="btn btn-sm btn-info btn-flat pull-left" value="<?php echo $_REQUEST['id']!=''?'Update':'Submit';?>">
                  <!--<a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>-->
                </div><!-- /.box-footer -->
                    <input type="hidden" name="control" value="master"/>
                    <input type="hidden" name="task" value="save_pickup_point"/>
                    <input type="hidden" name="id" id="idd" value="<?php echo $datas[0]['id']; ?>"  />
               </form>
               
                
                
                
              </div>
   
      
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

