<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;
global $conn;
?>
<div class="row">
   <div class="col-xs-8">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Class Fee Master</h3>
            <?php foreach($results as $result) { }  ?>
          <!--  <a href="index.php?control=master&task=addnew_class" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Class</a>-->
                        
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Class Fee Master List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15">
                              <div align="center">S.No</div>
                           </th>
                           <th><div align="center">Class</div></th>
                           <th> <div align="center">Fee Name</div></th>
                           <th><div align="center"><i class="fa fa-inr"></i> Fee Amount</div></th>
                           <th><div align="center">No. Of day Late</div></th>
                           <th><div align="center"><i class="fa fa-inr"></i> Late Fine</div></th>
                           <th><div align="center">Action</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           ?>
                        <tr>
                           <td align="center"><?php echo $countno; ?></td>                   
                           <td align="center"><?php echo $this->className($result['class_master_id']);?></td>
                           <td align="center"><?php echo $this->feetypeName($result['fee_type_id']);?></td>
                           <td align="center"><i class="fa fa-inr"></i> <?php echo $result['fee_amount'];?>/-</td>
                           <td align="center"><?php echo $result['day'];?></td>
                           <td align="center"><i class="fa fa-inr"></i> <?php echo $result['fee_fine'];?>/-</td>
                           
                           <td align="center">
                              <a href="index.php?control=master&task=class_fee_master&id=<?php echo $result['id']; ?>&class_master_id=<?php echo $result['class_master_id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
                              <?php
                                 if($result['status']==1){  ?>
                              <a href="index.php?control=master&task=status_class_fee_master&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                              <?php } else { ?>
                              <a href="index.php?control=master&task=status_class_fee_master&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                              <?php } ?>
                            
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
  
         <?php foreach($datas as $data) { }  ?>
   <div class="col-xs-4">
   <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php if($data['id']){echo "Edit";}else{ echo "Add";} ?> New Class Fee </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
       
                <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" > 
                <div class="box-body">
                <div class="form-group">
                    <label>Class <?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-pencil"></i>
                      </div>
                      
                         <select name="class_master_id" id="class_master_id"  class="form-control remove_radious select2">
                         <option value="">Select</option>
                         <?php $query = mysqli_query($conn,"SELECT * FROM `class_master` WHERE 1");
		                       while($res= mysqli_fetch_array($query)){   ?>
                          <option value="<?php echo $res['id']; ?>" <?php if($res['id']==$data['class_master_id']){ echo 'selected';} ?>><?php echo $res['name']; ?></option>
                          <?php } ?>
                          
                          
                         </select>
                       
                    </div>
                  </div>
                  
                <div class="form-group">
                    <label>Fee Type <?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-pencil"></i>
                      </div>
                    
                         <select name="fee_type_id" id="fee_type_id"  class="form-control remove_radious select2">
                         <option value="">Select</option>
                         <?php $feeSql = mysqli_query($conn,"SELECT * FROM `fee_type` WHERE 1");
		                       while($feeRes= mysqli_fetch_array($feeSql)){   ?>
                          <option value="<?php echo $feeRes['id']; ?>" <?php if($feeRes['id']==$data['fee_type_id']){ echo 'selected';} ?>><?php echo $feeRes['name']; ?></option>
                          <?php } ?>
                          </select>
                        
                    </div>
                  </div>
                
                
               
              <div class="form-group">
                    <label>Fee Amount<?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-pencil"></i>
                      </div>
                        <input type="text" onkeyup="calculate()" maxlength="6" value="<?php echo $data['fee_amount']; ?>" id="fee_amount" name="fee_amount" class="form-control remove_radious number_only"  required="">
                   
                    </div>
                  </div>
                  
                  
                <div class="form-group">
                    <label>No. Of Day Late(Fine) <?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-pencil"></i>
                      </div>
                      <input type="text" onkeyup="calculate()" maxlength="4" value="<?php echo $data['day']; ?>" id="day" name="day" class="form-control remove_radious number_only"  required="">
                   
                    </div>
                  </div>
                  
                  
                 <div class="form-group">
                    <label>No. Of Day Late Fine Amount <?php echo REQUIRED; ?></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-pencil"></i>
                      </div>
                      <input type="text" onkeyup="calculate()" maxlength="4" value="<?php echo $data['fee_fine']; ?>" id="fee_fine" name="fee_fine" class="form-control remove_radious number_only"  required="">
                   
                    </div>
                  </div>
               
                
             
                </div>
                 
                <div class="box-footer clearfix">
                 <input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php echo $_REQUEST['id']!=''?'Update':'Submit';?>">               
                <input type="hidden" name="control" value="master"/>
               <input type="hidden" name="task" value="save_class_fee_master"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $datas[0]['id']; ?>"  />
               </div>
               </form>
               
                
                
                
              </div>
   
      
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
   
   
   
   
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   
   function calculate(){
   admission_fee = parseInt($('#admission_fee').val());
   monthly_fee = parseInt($('#monthly_fee').val());
   admission_kit = parseInt($('#admission_kit').val());

   $('#total').val((admission_fee?admission_fee:0)
      +(monthly_fee?monthly_fee:0)
      +(admission_kit?admission_kit:0)
      );
} 
</script>

