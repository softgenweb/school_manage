<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style type="text/css">
  .li_padd{
    position: relative;
    display: block;
    padding: 10px 15px;
    border-bottom: 1px solid #f4f4f4 !important;
    cursor: pointer;
  }
  .li_padd a{
    /*padding: unset !important;*/
    display: inline !important;
    font-weight: bold;
  }
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Menu</h3>
            <?php foreach($results as $result) { }  ?>
            <a href="index.php?control=menu&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Menu</a>
           
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Menu List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                <div class="col-md-10 col-md-offset-1">
                  <ul class="nav nav-pills nav-stacked sortable main_menu" >
                  <?php
                    $i=0;
                   foreach($results as $result) {
                   $i++; ?>
                   <li class="li_padd" id="main_li<?php echo $i; ?>"><?php echo $i.'. '.$result['name']; ?>
                    <span class="pull-right">
                      <a href="index.php?control=menu&task=addnew&id=<?php echo $result['id']; ?>" >Edit</a>&nbsp;|&nbsp;
                       <?php if($result['parent']==1){ ?>
                      <a href="index.php?control=menu&task=addnew_chlid&pid=<?php echo $result['id']; ?>" >Add Child</a>
                    <?php } ?>
                    </span>
                  </li>
                   <?php if($result['parent']==1){ ?>
                      <div class="col-md-1 "></div>
                      <div class="col-md-11">
                      <ul class="nav nav-pills nav-stacked sortable child_menu">
                     <?php $this->Query("SELECT * FROM `child_menus` WHERE `parent`='".$result['id']."' AND `status`=1"); 
                      $cmenus = $this->fetchArray();   
                          $j=0;
                      foreach($cmenus as $cmenu) {
                        $j++;
                      ?>
                    <li class="li_padd" id="child_li<?php echo $j;?>"><?php echo  $j.'. '.$cmenu['name']; ?>
                      <span class="pull-right">
                        <a href="index.php?control=menu&task=addnew_chlid&id=<?php echo $cmenu['id']; ?>">Edit</a>
                      </span>
                    </li>
                  <?php } ?> 
                    </ul>
                </div>
                <div class="clearfix"></div>
                <?php }} ?>
              </ul>
                </div>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
/*main_menu = $('.main_menu > li').length;
console.log(main_menu);
child_menu = $('.child_menu > li').length;
console.log(child_menu);
*/

$('.main_menu > li').ready(function(){
  n = $('.main_menu > li').length;
  console.log(n);
   $(this).mousedown(function(){
      $(this).each(function(a){
    $(this).attr('id','main_li'+parseInt(a+1));
   });
   })
  
});
</script>

