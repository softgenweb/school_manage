<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Students</h3>
            <?php foreach($results as $result) { }  ?>
            <a href="index.php?control=student&task=addnew_admission_exam" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> New Registration</a>
           
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Student List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
               <div class="divoverflow">
                 
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div></th>
                           <!-- <th><div align="center">Registration No</div></th> -->
                           <th><div align="center">Name</div></th>
                           <th><div align="center">Class</div></th>
                           <th><div align="center">Gender</div></th>
                           <th><div align="center">Parent</div></th>
                           <th><div align="center">Birth Date</div></th>
                           <th><div align="center">Contact</div></th>
                           <th><div align="center">Exam Fee</div></th>
                           <!-- <th><div align="center">Result</div></th> -->
                           <th ><div align="center" >Action</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
							$final_result = ($result['exam_result']=='1')?'<span style="color:green;">Pass</span>':(
							($result['exam_result']=='2')?'<span style="color:red;">Fail</span>':(
							($result['exam_result']=='3')?'<span style="color:blue;">Admitted</span>':'Pending'));
                           ?>
                        <tr>
                           <td align="center"><strong><?php echo $countno; ?>.</strong></td>  
                           <td align="center"><strong><?php echo $this->admission_test_student($result['id']);?></strong></td>
                           <td align="center"><?php echo $this->className($result['class_id']);?></td>
                           <td align="center"><?php echo $result['gender'];?></td>
                           <td align="center"><?php echo $result['father_name'];?></td>
                           <td align="center"><?php echo $result['dob'];?></td>
                           <td align="center"><?php echo $result['parents_mobile'];?></td>
                           <td align="center"><?php echo $result['exam_fee'];?></td>
                           <!-- <td align="center"><strong><?php echo $final_result;?></strong></td> -->
                           <td align="center" style="display: inline-flex;"> 
                            <a href="index.php?control=student&task=addnew_admission_exam&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a> &nbsp; &nbsp;
                              <?php if($result['exam_result']=='0'){ ?>
                               
                              <form method="post" action="">
                                 <select class="form-control" name="exam_result">
                                    <option value="">Select</option>
                                    <option value="1" <?php echo $result['exam_result']=='1'?'selected':''; ?>>Pass</option>
                                    <option value="2" <?php echo $result['exam_result']=='2'?'selected':''; ?>>Fail</option>
                                 </select>
                                 <input type="submit" value="Submit" name="change_status" class="btn btn-primary bulu">
                                 <input type="hidden" name="control" value="student">
                                 <input type="hidden" name="id" value="<?php echo $result['id'];?>">
                                 <input type="hidden" name="task" value="status_admission_exam">
                              </form>
                            <?php }else{ ?>
                                 <strong><?php echo $final_result;?></strong>
                           <?php  } if($result['exam_result']=='1'){ ?>
                              &nbsp; &nbsp; <a href="index.php?control=student&task=show&rege_id=<?php echo $result['id']; ?>" class="btn btn-primary redu" style="cursor:pointer;" title="Edit"><b>Admission</b></a> 
                            <?php } ?>
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
</script>

