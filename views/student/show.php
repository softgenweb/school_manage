<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style >
.select2-container {
	display: block;
}
</style>

<div class="row">
	  
  <?php
   foreach($datas as $data) { }  
   $admission_test_id = $_REQUEST['rege_id'];
if($admission_test_id!=''){
	$data=mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `admission_test` WHERE `id`='".$admission_test_id."' AND `exam_result`='1'"));
}?>
   <div class="col-xs-12">
   <div class="box box-info <?php if(!$data['id']){?> collapsed-box<?php } ?>">
  <!-- <div class="box box-info">-->

  
                <div class="box-header with-border">
                  <h3 class="box-title" data-widget="collapse" style="cursor:pointer"> <?php if($data['id']){echo "Edit";}else{echo "Add New";}?>  Admission Form</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-<?php if($data['id']){?>minus<?php }else{ echo "plus";} ?>"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
       
                <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" > 
                <div class="box-body">
              <div class="row col-md-12">
				<h4 class="col-md-10 col-md-offset-1"><b>1. <u>Information of the Child :</u></b></h4>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Class <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="class_id" id="class_id" class="form-control select2" required="">
								<option value="">Select</option>
								<?php $this->Query("SELECT * FROM `class_master` WHERE `status`=1");
								$classes = $this->fetchArray();
								foreach($classes as $class){
								 ?>
								 <option value="<?php echo $class['id']; ?>" <?php echo $class['id']==$data['class_id']?'selected':''; ?>><?php echo $class['name']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
                    <div class="clearfix"></div>
                    
                    	<div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Section <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="section_id" id="section_id" class="form-control select2" required="">
								<option value="">Select</option>
								<?php $this->Query("SELECT * FROM `class_section` WHERE `status`=1");
								$sections = $this->fetchArray();
								foreach($sections as $section){
								 ?>
								 <option value="<?php echo $section['id']; ?>" <?php echo $section['id']==$data['section_id']?'selected':''; ?>><?php echo $section['name']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
                    <div class="clearfix"></div>
                    
                    
					<div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>First Name <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['fname']?$data['fname']:$data['first_name']; ?>" pattern="[A-Za-z\s]+" id="fname" name="fname" class="form-control"  required="">
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Middle Name</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['mname']?$data['mname']:$data['middle_name']; ?>" pattern="[A-Za-z\s]+" id="mname" name="mname" class="form-control" >
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Last Name <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['lname']?$data['lname']:$data['last_name']; ?>" pattern="[A-Za-z\s]+" id="lname" name="lname" class="form-control"  required="">
						</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Gender <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="gender" id="gender" class="form-control">
								<option value="">Select</option>
								<option value="Male" <?php echo $data['gender']=="Male"?"selected":""; ?>>Male</option>
								<option value="Female" <?php echo $data['gender']=="Female"?"selected":""; ?>>Female</option>
							</select>
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Date of Birth <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['dob']; ?>" id="dob" name="dob" class="form-control datepicker" placeholder="dd/mm/yyyy" readonly="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Blood Group </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['blood_group']; ?>" id="blood_group" name="blood_group" class="form-control" >
							
						</div>
					</div>
                    <div class="clearfix"></div>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Religion <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['religion']; ?>" id="religion" name="religion" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Caste</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['caste']; ?>" id="caste" name="caste" class="form-control"  >
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Nationality <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['nationality']?$data['nationality']:'INDIAN'; ?>" id="nationality" name="nationality" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Aadhar No </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" maxlength="12" value="<?php echo $data['aadhar_no']; ?>" id="aadhar_no" name="aadhar_no" class="form-control"  >							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Community <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="community" id="community" class="form-control">
								<option value="">Select</option>
								<option value="General" <?php echo $data['community']=="General"?"selected":""; ?>>General</option>
								<option value="OBC" <?php echo $data['community']=="OBC"?"selected":""; ?>>OBC</option>
								<option value="SC/ST" <?php echo $data['community']=="SC/ST"?"selected":""; ?>>SC/ST</option>
								<option value="Other" <?php echo $data['community']=="Other"?"selected":""; ?>>Other</option>
							</select>
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Languages known</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['known_language']; ?>" id="known_language" name="known_language" class="form-control" >
							
						</div>
					</div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Mother Tongue</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['mother_tongue']; ?>" id="mother_tongue" name="mother_tongue" class="form-control" >
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Student Photo  <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="file" value="<?php //echo $data['student_photo']; ?>" id="student_photo" name="student_photo" class="form-control" <?php if($data['id']==''){echo "required";} ?>>
							
						</div>
					</div>
                    <div class="clearfix"></div>
				</div> 
				<div class="clearfix"></div>
				<h4 class="col-md-10 col-md-offset-1"><b>2. <u> Address :</u></b></h4>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-2"></div>
					<div class="col-md-4"><b><u>Residential</u> :</b></div>
				 <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Address <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea id="residential_address" name="residential_address" class="form-control"  required=""><?php echo $data['residential_address']; ?></textarea>
							<!-- <input type="text" value="<?php echo $result['url']; ?>"> -->
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Parent Mobile <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" pattern="[6789][0-9]{9}" maxlength="10" value="<?php echo $data['parent_mobile']?$data['parent_mobile']:$data['parents_mobile']; ?>" id="parent_mobile" name="parent_mobile" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Father Name <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['father_name']; ?>" id="father_name" name="father_name" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Mother Name <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['mother_name']; ?>" id="mother_name" name="mother_name" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-4"><b><u>Correspondence</u> : </b> </div>
					<div class="col-md-6"><input type="checkbox" name="same_address" id="same_address"> Same as Residential</div>
					<div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Address <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea id="correspondence_address" name="correspondence_address" class="form-control"  required=""><?php echo $data['correspondence_address']; ?></textarea>
							<!-- <input type="text" value="<?php echo $data['url']; ?>"> -->
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Guardian Mobile <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" pattern="[6789][0-9]{9}" maxlength="10" value="<?php echo $data['guardian_mobile']; ?>" id="guardian_mobile" name="guardian_mobile" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Guardian Name <?php echo REQUIRED; ?></label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $data['guardian_name']; ?>" id="guardian_name" name="guardian_name" class="form-control"  required="">
							
						</div>
					</div>
                  </div> 		
              
                <div class="clearfix"></div>
				<h4 class="col-md-10 col-md-offset-1"><b>3. <u> Transport :</u></b></h4>
				<div class="col-md-6 col-xs-12">
				 <div class="clearfix"></div>
					<div class="col-md-6" align="right">
                       <div class="form-group center_text">
							<label>School Bus Transport </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="transport" id="transport"  value="1" <?php echo ($data['transport']==1)?'checked':''; ?> class="checkbox" style="margin-top: 15px;" onclick="oncheckTransport();" />
							
						</div>
					</div>
                    <div class="clearfix"></div>	
                    
                    <div id="hideTransport" style="display:<?php echo ($data['transport']==1)?'block':'none'; ?>;">
                     <div class="col-md-2"></div>
                    <div class="col-md-4">
                       <div class="form-group center_text">
							<label>Pick-up Point </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
						<select name="pickup_point_id" id="pickup_point_id" class="form-control select2" >
								<option value="">Select</option>
								<?php echo $this->Query("SELECT * FROM `pickup_point` WHERE `status`=1");
								$pickup_points = $this->fetchArray();
								foreach($pickup_points as $pickup_point){
								 ?>
								 <option value="<?php echo $pickup_point['id']; ?>" <?php echo $pickup_point['id']==$data['pickup_point_id']?'selected':''; ?>><?php echo $pickup_point['name']."   ---> Fare (Rs. ".$pickup_point['amount'].")"; ?></option>
								<?php } ?>
							</select>				
						</div>
					</div>
                    <div class="clearfix"></div>
                    </div>
                   
				</div>    
                  		
			
			</div>
                </div>
                 
                <div class="box-footer clearfix" align="center">
               <input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['rege_id']!=''){ echo 'Update';}else{echo 'Submit';}?>">           
                 	<input type="hidden" name="control" value="student"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $datas[0]['id']; ?>"  />
					<input type="hidden" name="admission_test_id" value="<?php echo $admission_test_id; ?>"  />
               </div>
               </form>
               
                
                
                
              </div>
   
      
      <!-- /.box -->
   </div>

   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">View Students</h3>
            <?php foreach($results as $result) { }  ?>
          <!--  <a href="index.php?control=student&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add New Student</a>
           -->
            <!--<a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_country.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;" /></a> -->              
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Student List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    unset($_SESSION['alertmessage']);
            unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
               <div class="divoverflow">
                 
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No</div></th>
                           <th><div align="center">Admission No</div></th>
                           <th><div align="center">Name</div></th>
                           <th><div align="center">Image</div></th>
                           <th><div align="center">Class</div></th>
                           <th><div align="center">Gender</div></th>
                           <th><div align="center">Birth Date</div></th>
                           <th><div align="center">Contact</div></th>
                           <th><div align="center">Address</div></th>
                           <th><div align="center">Action</div></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           
                           ?>
                        <tr>
                           <td align="center"><strong><?php echo $countno; ?>.</strong></td>                  
                           <td align="center">
                              <a href="index.php?control=student&task=show_detail&admission_no=<?php echo $result['admission_no']; ?>">
                                 <strong><?php echo $result['admission_no'];?></strong>
                              </a>
                           </td>
                           <td align="center"><strong><?php echo $this->StudentName($result['id']);?></strong></td>
                           <td align="center"><strong><img src="<?php echo $result['student_photo'];?>" alt="" style="width:50px;" /></strong></td>
                           <td align="center"><a href="index.php?control=student&task=admission_fee&admission_no=<?php echo $result['admission_no']; ?>"><?php echo $this->className($result['class_id']);?> </a></td>
                           <td align="center"><?php echo $result['gender'];?></td>
                           <td align="center"><?php echo $result['dob'];?></td>
                           <td align="center"><?php echo $result['guardian_mobile'];?></td>
                           <td align="center"><?php echo $result['correspondence_address'];?></td>
                           <td align="center">
                              <a href="index.php?control=student&task=fee_details&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Fee Details</b></a>&nbsp;
                              <a href="index.php?control=student&task=show&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Edit"><b>Edit</b></a>&nbsp;
                              <?php
                                 if($result['status']==1){  ?>
                              <a href="index.php?control=student&task=status&status=0&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Inactive"><b style="color:green;cursor:pointer;" onclick="return confirm('Are you sure you want to Inactivate ?')">Active</b></a>
                              <?php } else { ?>
                              <a href="index.php?control=student&task=status&status=1&id=<?php echo $result['id']; ?>" style="cursor:pointer;" title="Click to Active"><b style="color:red;cursor:Confirm;" onclick="return confirm('Are you sure you want to Activate ?')">In-Active</b></a>
                              <?php } ?>
                            
                           </td>
                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<!-- /.row -->
<script>

function oncheckTransport(){
	str = document.getElementById('transport').checked;
	
	//alert(str); 
   if(str==true){
	document.getElementById('hideTransport').style.display = 'block';   
	//document.getElementById('pickup_point_id').value = '';   
   }else{
	document.getElementById('hideTransport').style.display = 'none';  
	document.getElementById('pickup_point_id').value = '';    
   }
}


   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   
$(document).ready(function() {
	$('#same_address').click(function(){
		if($(this).is(':checked')){
			res_address = $('#residential_address').val();
			parent_mobile = $('#parent_mobile').val();
			father_name = $('#father_name').val();

			$('#correspondence_address').val(res_address);
			$('#guardian_mobile').val(parent_mobile);
			$('#guardian_name').val(father_name);
		}else{
			$('#correspondence_address').val('');
			$('#guardian_mobile').val('');
			$('#guardian_name').val('');
		}
	});
});
$('.select2').select2();
</script>

