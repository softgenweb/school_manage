<?php foreach($results as $result) { }  ?>
<style type="text/css">
	.mendentory{
		color: #FF0000;
		font-size: 18px;
	}
</style>
	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Show Student Detail</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=student&task=show"><i class="fa fa-list" aria-hidden="true"></i> Student List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> View Student Detail</li>
       
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
            
	</div>
    		
      <div class="row">
            <div class="col-md-4">
              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user-image">
                    <img class="img-circle" src="<?php echo $result['student_photo']; ?>" alt="User Avatar">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username"><?php echo $this->StudentName($result['id']); ?></h3>
                  <h5 class="widget-user-desc">Class - <?php echo $this->className($result['class_id']); ?></h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Gender <span class="pull-right badge bg-blue"><?php echo $result['gender']; ?></span></a></li>
                    <li><a href="#">Date of Birth <span class="pull-right badge bg-aqua"><?php echo $result['dob']; ?></span></a></li>
                    <li><a href="#">Blood Group <span class="pull-right badge bg-green"><?php echo $result['blood_group']; ?></span></a></li>
                    <li><a href="#">Religion <span class="pull-right badge bg-red"><?php echo $result['religion']; ?></span></a></li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
            </div><!-- /.col -->
            
            
            <div class="row col-md-8">
            <div class="box box-widget">
            
				<h4 class="col-md-12"><b>1. <u>Information of the Child :</u></b></h4>
				<div class="col-md-6 col-xs-12">
					
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Class </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="class_id" id="class_id" class="form-control select2" required="">
								<option value="">Select</option>
								<?php $this->Query("SELECT * FROM `class_master` WHERE `status`=1");
								$classes = $this->fetchArray();
								foreach($classes as $class){
								 ?>
								 <option value="<?php echo $class['id']; ?>" <?php echo $class['id']==$result['class_id']?'selected':''; ?>><?php echo $class['name']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
                    <div class="clearfix"></div>
					
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>First Name </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['fname']; ?>" pattern="[A-Za-z\s]+" id="fname" name="fname" class="form-control"  required="">
						</div>
					</div>
                    <div class="clearfix"></div>
					
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Middle Name</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['mname']; ?>" pattern="[A-Za-z\s]+" id="mname" name="mname" class="form-control" >
						</div>
					</div>
                    <div class="clearfix"></div>
					
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Last Name </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['lname']; ?>" pattern="[A-Za-z\s]+" id="lname" name="lname" class="form-control"  required="">
						</div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Gender </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="gender" id="gender" class="form-control">
								<option value="">Select</option>
								<option value="Male" <?php echo $result['gender']=="Male"?"selected":""; ?>>Male</option>
								<option value="Female" <?php echo $result['gender']=="Female"?"selected":""; ?>>Female</option>
							</select>
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Date of Birth </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['dob']; ?>" id="dob" name="dob" class="form-control datepicker" placeholder="dd/mm/yyyy" readonly="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Blood Group </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['blood_group']; ?>" id="blood_group" name="blood_group" class="form-control" >
							
						</div>
					</div>
                    <div class="clearfix"></div>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Religion </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['religion']; ?>" id="religion" name="religion" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Caste</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['caste']; ?>" id="caste" name="caste" class="form-control"  >
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Nationality </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['nationality']?$result['nationality']:'INDIAN'; ?>" id="nationality" name="nationality" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Aadhar No </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" maxlength="12" value="<?php echo $result['aadhar_no']; ?>" id="aadhar_no" name="aadhar_no" class="form-control"  >							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Community </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<select name="community" id="community" class="form-control">
								<option value="">Select</option>
								<option value="General" <?php echo $result['community']=="General"?"selected":""; ?>>General</option>
								<option value="OBC" <?php echo $result['community']=="OBC"?"selected":""; ?>>OBC</option>
								<option value="SC/ST" <?php echo $result['community']=="SC/ST"?"selected":""; ?>>SC/ST</option>
								<option value="Other" <?php echo $result['community']=="Other"?"selected":""; ?>>Other</option>
							</select>
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Languages known</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['known_language']; ?>" id="known_language" name="known_language" class="form-control" >
							
						</div>
					</div>
                     <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Mother Tongue</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['mother_tongue']; ?>" id="mother_tongue" name="mother_tongue" class="form-control" >
							
						</div>
					</div>
                    <div class="clearfix"></div>
				</div> 
				<div class="clearfix"></div>
				<h4 class="col-md-12"><b>2. <u> Address :</u></b></h4>
				<div class="col-md-6 col-xs-12">
					
					<div class="col-md-4"><b><u>Residential</u> :</b></div>
				 <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Address </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea id="residential_address" name="residential_address" class="form-control"  required=""><?php echo $result['residential_address']; ?></textarea>
							<!-- <input type="text" value="<?php echo $result['url']; ?>"> -->
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Parent Mobile </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" pattern="[6789][0-9]{9}" maxlength="10" value="<?php echo $result['parent_mobile']; ?>" id="parent_mobile" name="parent_mobile" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Father Name </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['father_name']; ?>" id="father_name" name="father_name" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Mother Name </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['mother_name']; ?>" id="mother_name" name="mother_name" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-4"><b><u>Correspondence</u> : </b> </div>
					<!-- <div class="col-md-6"><input type="checkbox" name="same_address" id="same_address"> Same as Residential</div> -->
					<div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Address </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea id="correspondence_address" name="correspondence_address" class="form-control"  required=""><?php echo $result['correspondence_address']; ?></textarea>
							<!-- <input type="text" value="<?php echo $result['url']; ?>"> -->
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Guardian Mobile </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" pattern="[6789][0-9]{9}" maxlength="10" value="<?php echo $result['guardian_mobile']; ?>" id="guardian_mobile" name="guardian_mobile" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group center_text">
							<label>Guardian Name </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" value="<?php echo $result['guardian_name']; ?>" id="guardian_name" name="guardian_name" class="form-control"  required="">
							
						</div>
					</div>
                    <div class="clearfix"></div>
				</div> 				
				<div class="clearfix"></div>
				
	
                </div>
			</div>
            
            
          
          </div>      
            
        
            
                          
	
                 
              
     
<script>

	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});

   
function goBack() {
    window.history.back();
}

$(document).ready(function() {
	$('#same_address').click(function(){
		if($(this).is(':checked')){
			
			res_address = $('#residential_address').val();
			parent_mobile = $('#parent_mobile').val();
			father_name = $('#father_name').val();

			$('#correspondence_address').val(res_address);
			$('#guardian_mobile').val(parent_mobile);
			$('#guardian_name').val(father_name);
		}else{
			$('#correspondence_address').val('');
			$('#guardian_mobile').val('');
			$('#guardian_name').val('');
		}
	});
});
// $('.select2').select2();

$('input').prop('required',false).prop('disabled',true);
$('select').prop('required',false).prop('disabled',true);
$('textarea').prop('required',false).prop('disabled',true);
</script>








