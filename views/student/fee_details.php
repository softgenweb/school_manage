<?php foreach($results as $result) { }  ?>
<style type="text/css">
	.mendentory{
		color: #FF0000;
		font-size: 18px;
	}
</style>
	<div class="panel panel-default" >
		<div class="box-header">        
		<h3 class="box-title">Student Fee Detail</h3>
	</div> 
	<ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
            <li><a href="index.php?control=student&task=show"><i class="fa fa-list" aria-hidden="true"></i> Student List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Student Fee Detail</li>
       
          </ol>
          
       <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
                <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
                </div>
       </div>
       
      <?php   	unset($_SESSION['alertmessage']);
                unset($_SESSION['errorclass']);    
	   }?>
         
         
         
	<div class="panel-body">
         <div class="row col-md-12">
				<h4 class="col-md-10 col-md-offset-1"><b>1. <u>Information of the Child :</u></b></h4>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Class </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $this->className($result['class_id']); ?></div>
					</div>
                    <div class="clearfix"></div>
                    
					<div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Name </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['fname'].' '.$result['mname'].' '.$result['lname']; ?>
						</div>
					</div>
                    <div class="clearfix"></div>
					
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Gender </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['gender']; ?></div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Date of Birth </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['dob']; ?></div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Blood Group </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['blood_group']; ?></div>
					</div>
                    <div class="clearfix"></div>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Religion</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['religion']; ?></div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Caste</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['caste']; ?></div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Nationality </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['nationality']?$result['nationality']:'INDIAN'; ?>
							
						</div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Aadhar No </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['aadhar_no']; ?></div>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Community </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['community']; ?></div>
					</div>
                	
				</div> 
				<div class="clearfix"></div>
                
                
				<h4 class="col-md-10 col-md-offset-1"><b>2. <u> Admission Fees :</u></b></h4>
				<div class="col-md-6 col-xs-12">
					
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Address Fee</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><i class="fa fa-inr"></i> <?php echo $result['admission_fee']; ?>/-</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Admission KIT</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><i class="fa fa-inr"></i> <?php echo $result['admission_kit']; ?>/-</div>
					</div>
                    <div class="clearfix"></div>
                    <div class="col-md-2"></div>
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Monthly Fee</label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><i class="fa fa-inr"></i> <?php echo $result['monthly_fee']; ?>/-</div>
					</div>
                    <div class="clearfix"></div>
                    
                    
                   
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="col-md-4">
                       <div class="form-group ">
							<label>Address </label>
						</div>	
                   </div>
					<div class="col-md-6">
						<div class="form-group"><?php echo $result['correspondence_address']; ?></div>
					</div>
                    <div class="clearfix"></div>
					
				
				</div> 				
				<div class="clearfix"></div>
				<center>
					<a href="index.php?control=student&task=show" class="btn btn-primary">Back</a>
				</center>
				<!-- <div class="col-md-12 col-sm-12 col-xs-12">    
					<center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
					<input type="hidden" name="control" value="student"/>
					<input type="hidden" name="edit" value="1"/>
					<input type="hidden" name="task" value="save"/>
					<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
				</div>   -->  

			</div>

	</div>
                 
                  </div>
                </div><!-- table-responsive -->
                </div>
                      
                     </div>
     
<script>

	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});

   
function goBack() {
    window.history.back();
}

$(document).ready(function() {
	$('#same_address').click(function(){
		if($(this).is(':checked')){
			
			res_address = $('#residential_address').val();
			parent_mobile = $('#parent_mobile').val();
			father_name = $('#father_name').val();

			$('#correspondence_address').val(res_address);
			$('#guardian_mobile').val(parent_mobile);
			$('#guardian_name').val(father_name);
		}else{
			$('#correspondence_address').val('');
			$('#guardian_mobile').val('');
			$('#guardian_name').val('');
		}
	});
});
// $('.select2').select2();

$('input').prop('required',false).prop('disabled',true);
$('select').prop('required',false).prop('disabled',true);
$('textarea').prop('required',false).prop('disabled',true);
</script>








