<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Class</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=student&task=show_admission_exam"><i class="fa fa-list" aria-hidden="true"></i> Student List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Registration</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> New Registration</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php   	unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Class <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="class_id" id="class_id" class="form-control select2" required="">
                        <option value="">Select</option>
                        <?php $this->Query("SELECT * FROM `class_master` WHERE `status`=1");
                        $classes = $this->fetchArray();
                        foreach($classes as $class){
                         ?>
                         <option value="<?php echo $class['id']; ?>" <?php echo $class['id']==$result['class_id']?'selected':''; ?>><?php echo $class['name']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>First Name <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $result['first_name']; ?>" id="first_name" name="first_name" class="form-control"  required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Middle Name </label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $result['middle_name']; ?>" id="middle_name" name="middle_name" class="form-control"  required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Last Name <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $result['last_name']; ?>" id="last_name" name="last_name" class="form-control"  required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Father Name  <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['father_name']; ?>" id="father_name" name="father_name" class="form-control"  required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Father Mobile  <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" value="<?php echo $result['parents_mobile']; ?>" id="parents_mobile"  pattern="[6789][0-9]{9}" name="parents_mobile" class="form-control"  required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Date of Birth  <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" value="<?php echo $result['dob']; ?>" id="dob" name="dob" class="form-control datepicker" placeholder="dd/mm/yyyy" readonly="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Gender  <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="gender" id="gender" class="form-control">
                        <option value="">Select</option>
                        <option value="Male" <?php echo $result['gender']=="Male"?"selected":""; ?>>Male</option>
                        <option value="Female" <?php echo $result['gender']=="Female"?"selected":""; ?>>Female</option>
                     </select>
                     
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Exam Fee  <?php echo REQUIRED; ?></label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" maxlength="10" value="<?php echo $result['exam_fee']; ?>" id="exam_fee" name="exam_fee" class="form-control number_only"  required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php echo $_REQUEST['id']!=''?'Update':'Submit';?>"></center>
               </div>
               <input type="hidden" name="control" value="student"/>
               <input type="hidden" name="task" value="save_admission_exam"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<script type="text/javascript">
   function validation()
   
   {   
   	var chk=1;
   	
   		if(document.getElementById('country_name').value == '') { 
   			document.getElementById('msgcountry_name').innerHTML = "*Required field.";
   			chk=0;
   		}
   		else if(!isletter(document.getElementById('country_name').value)) { 
   			document.getElementById('msgcountry_name').innerHTML = "*Enter Valid Country Name.";
   			chk=0;
   		}
   		
   		else {
   			document.getElementById('msgcountry_name').innerHTML = "";
   		}
   
   			if(chk)
   			 {			
   				return true;		
   			}		
   			else 
   			{		
   				return false;		
   			}	
   		
   		
   		}
   
</script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
   	$(this).alert('close');
   });
$('.number_only').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
  });

     
$('.select2').select2();
</script>

