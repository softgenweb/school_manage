<?php session_start();?>
<aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;">
   <div class="background_layer"></div>
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <div class="user-panel" style="padding-bottom: 48px;">
         <div class="pull-left image">
            <img src="<?php echo $_SESSION['user_img']?'media/user_img/'.$_SESSION['user_img']:'images/user2-160x160.jpg'; ?>" class="img-circle" alt="User Image"></a>
         </div>
         <div class="pull-left info" >
            <h3 style="margin:unset;"><?php echo $_SESSION['admin_name']; ?></h3>
            <a href="index.php?control=user&task=updateprofile&aid=<?php echo $_SESSION['adminid'];?>" title="Update Profile"><i class="fa fa-circle text-success"></i><b>Update Profile</b></a>&nbsp;</br>
            <!-- <a href="index.php?control=user&task=changepassword" title="Change Password"><i class="fa fa-circle text-success"></i><b>Change Password</b></a><br /> -->
         </div>
      </div>
      <ul class="sidebar-menu">
         <?php 
            if($_SESSION['utype']=='Administrator'){
               $query = mysqli_query($conn, "SELECT * FROM `menus` WHERE 1");
             }else{
               $query = mysqli_query($conn, "SELECT * FROM `menus` WHERE `id` IN (".$_SESSION['permission'].") AND `status`=1");
             }
                 
                  while($menu = mysqli_fetch_array($query)){
                     if($menu['parent']=="0"){
                     ?>
         <li <?php if($_REQUEST['control']==$menu['control'] && $_REQUEST['task']==$menu['task']){ ?>class="treeview active active-menu"<?php } ?>>
            <a href="<?php echo $menu['url']; ?>">
            <i class="fa fa-<?php echo $menu['icon']; ?>" aria-hidden="true"></i>
            <span><?php echo $menu['name']; ?></span>
            </a>
         </li>
         <?php }else{ ?>
         <li class="treeview <?php if($_REQUEST['control']==$menu['control'] ){ ?>active<?php } ?>">
            <a href="javascript:;">
            <i class="fa fa-<?php echo $menu['icon']; ?>" aria-hidden="true"></i>
            <span><?php echo $menu['name']; ?></span>
            </a>
            <ul class="treeview-menu">
               <?php $sql = mysqli_query($conn, "SELECT * FROM `child_menus` WHERE `parent`='".$menu['id']."' AND `status`=1 ORDER BY `name` ASC");
                  while($cmenu = mysqli_fetch_array($sql)){
                      ?>
               <li <?php if($_REQUEST['control']==$cmenu['control'] && $_REQUEST['task']==$cmenu['task']){ ?>class="active"<?php } ?>><a href="<?php echo $cmenu['url']; ?>">
			   <i class="fa fa-<?php echo $cmenu['icon']; ?>" aria-hidden="true"></i>
			   <?php echo $cmenu['name']; ?></a></li>
               <?php } ?>
            </ul>
         </li>
         <?php } } ?>
      </ul>

   </section>
   <!-- /.sidebar -->
</aside>

