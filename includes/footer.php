 <!--   <footer class="main-footer">
        <div class="pull-right hidden-xs">
         
        </div>
         Copyright &copy; 2017 All rights reserved.<strong> <a href="http://softgentechnologies.com/" target="_blank">SOFTGEN TECHNOLOGIES PVT. LTD.</a></strong> 
      </footer>-->

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
  
   <!-- jQuery 2.1.4 -->

    <!-- <script src="<?php echo $tmp;?>/plugins/jQuery/jQuery-2.1.4.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $tmp;?>/bootstrap/js/bootstrap.min.js"></script>
     <!-- DataTables -->
    <script src="<?php echo $tmp;?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $tmp;?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
    <!-- <script src="<?php echo $tmp;?>/plugins/morris/morris.min.js"></script> -->
    <!-- Sparkline -->
    <!-- <script src="<?php echo $tmp;?>/plugins/sparkline/jquery.sparkline.min.js"></script> -->
    <!-- jvectormap -->
    <!-- <script src="<?php echo $tmp;?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> -->
    <!-- <script src="<?php echo $tmp;?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> -->
    <!-- jQuery Knob Chart -->
    <!-- <script src="<?php echo $tmp;?>/plugins/knob/jquery.knob.js"></script> -->
    <!-- daterangepicker -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> -->
    <script src="<?php echo $tmp;?>/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo $tmp;?>/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo $tmp;?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo $tmp;?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $tmp;?>/plugins/fastclick/fastclick.min.js"></script> 
       <!-- AdminLTE App -->
    <script src="<?php echo $tmp;?>/dist/js/app.min.js"></script>
   
      <script>
        $('.datepicker').datepicker({
          format:'dd/mm/yyyy',
          autoclose: true,
          endDate: '-10m'
        });
      $(function () {
        $("#example1-1").DataTable({
           "iDisplayLength": 25
        });
        $('#example1-2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

	  $(function () {
        $("#example1-3").DataTable();
        $('#example1-4').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });

$( ".sortable" ).sortable();
$( ".sortable" ).disableSelection();

$('a.logout').click(function () {
    document.cookie ="googtrans=/en/en;  path=/";
});

$(document).ready(function(){
  $('.goog-te-combo').addClass('select2');

});
  $('.select2').select2();
</script>
  </body>
</html>
