<?php

require_once('createthumb.php');
if(file_exists('textconfig/config.php')) {
    require_once('textconfig/config.php');
}
else {
    require_once('../textconfig/config.php');
}
if(file_exists('configuration.php')){
    require_once('configuration.php');
}
else {
    require_once('../configuration.php');
}
class DbAccess extends  CreateThumb {
    public $result;
    public $query;
    public $num_row;
    public $tmpPath;
    public $taxonomy_cont;
    public function DbAccess(){


        if($_SESSION['username']){
            $query = $tmpid?"SELECT * FROM templates WHERE id = '".$tmpid."' AND tmp_type = 0 AND  status = '1'":"SELECT * FROM templates WHERE status = '1' AND tmp_type = 0 AND default_temp='1'";
            $this->Query($query);
            $result = $this->fetchArray();
            $this->tmpPath = "template/".$result[0]['name'];
        }else {
            $query = "SELECT * FROM templates WHERE status = '1' AND tmp_type = 1 AND default_temp='1'";
            $this->Query($query);
            $result = $this->fetchArray();
            $this->tmpPath = "template/".$result[0]['name'];

        }
    }
    function getTemplate($tmpid) {
        if($_SESSION['username']){
            $query = $tmpid?"SELECT * FROM templates WHERE id = '".$tmpid."' AND tmp_type = 0 AND  status = '1'":"SELECT * FROM templates WHERE status = '1' AND tmp_type = 0 AND default_temp='1'";
            $this->Query($query);
            return $this->fetchArray();
        }
        else {
            $query = $tmpid?"SELECT * FROM templates WHERE id = '".$tmpid."' AND tmp_type = 1 AND  status = '1'":"SELECT * FROM templates WHERE status = '1' AND tmp_type = 1 AND default_temp='1'";
            $this->Query($query);
            return $this->fetchArray();

        }

    }

    /*function cityByCountryId($id) {
        $q_city = "SELECT id,city FROM cities  WHERE country_id = '".$id."' and status = '1' and language_id = '1'";
        $this->Query($q_city);
        $data = $this->fetchArray();
        return $data;
    }
    function cityById($id) {
        $q_city = "SELECT id,city FROM cities  WHERE id = '".$id."' and language_id = '1'";
        $this->Query($q_city);
        $data = $this->fetchArray();
        return $data[0]['city'];
    }
    function countryByCityId($id) {
        $q_country_by_city = "SELECT cntr.* FROM countries cntr JOIN cities c ON cntr.id = c.country_id  WHERE c.id = '".$id."' and cntr.language_id = '1'";
        $this->Query($q_country_by_city);
        $country_by_city = $this->fetchArray();
        return $country_by_city[0]['id'];
    }

    function getCountry($id) {
        $q_country = "SELECT * FROM countries WHERE id = '".$id."'  AND language_id = '1'";
        $this->Query($q_country);
        $country = $this->fetchArray();
        return $country[0]['country'];
    }*/


    function getMenuTop() {
        /*$query = "SELECT * FROM menus WHERE status = '1'";
        $this->Query($query);*/
        return $this->fetchArray();

    }

/**************Start Direct Query Exceute Format****************/

    function sqlQuery($str) {
        $this->query =  "SELECT * FROM ".$str." WHERE 1";
        return true;
    }


    function Query($str) {
        $this->query = $str;
        return true;
    }

    function Execute() {
        global $conn;
        $this->result = mysqli_query($conn, $this->query) or die(mysqli_connect_error());
        return $this->result;
    }

    function isExecute() {
        global $conn;
        return  mysqli_query($conn, $this->query);
    }

    function fetchArray() {
        $this->Execute();
        $this->NumRow();
        for($i=0; $i<$this->num_row; $i++){
            $fetch_result[$i] = mysqli_fetch_array($this->result);
        }
        return $fetch_result;
    }

    function fetchObject() {
        $this->Execute();
        $this->NumRow();
        for($i=0; $i<$this->num_row; $i++){
            $fetch_result[$i] = mysqli_fetch_object($this->result);
        }
        return $fetch_result;
    }

    function numRow() {
        $this->num_row = mysqli_num_rows($this->result);
        return $this->num_row;
    }

    function rowCount(){
        global $conn;
        $this->result = mysqli_query($conn, $this->query) or die(mysqli_connect_error());
        $this->num_row = mysqli_num_rows($this->result);
        return $this->num_row;
    }

    /**************Start Direct Query Exceute Format****************/


    function mailsend($to,$from=NULL,$subject=NULL,$message=NULL) {

        $from = $from?$from:EMAILFROM; //$fromMail;//
        $subject = $subject?$subject:SUBJECTMAIL;
        $message = $message?$message:"<a href=".$this->token.">Click here to varify your account</a>";
        //////////////////////////////////////////////////



        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.SUBJECTMAIL.' '.$from;

        // Additional headers
        //$headers .= 'To: '. $from. "\r\n";
        //$headers .= 'From: '.$subject.' <'.$to.'>' . "\r\n";

        $ok = @mail($to,$subject, $message,  $headers);
        if ($ok) {
            return true;
        } else {
            return false;
        }
    }

    function uploadFile($dest,$file,$type = NULL) {
        if($type) {
            if($file['type']==$type) {
                $des = $dest."/".$_REQUEST['id'].$file['name'];
                if(move_uploaded_file($file['tmp_name'],$des)) {
                    return $des;
                }
                else {
                    return NULL;
                }
            }
            else {
                return NULL;
            }
        }
        else {
            $des = $dest."/".$_REQUEST['id'].$file['name'];
            if(move_uploaded_file($file['tmp_name'],$des)) {
                return $des;
            }
            else {
                return NULL;
            }
        }
    }
    //Language code start here
    function  taxolist() {

        return $arr;
    }
    function language() {
        /*$q_show = "SELECT id,content FROM languages  WHERE deff = '1' and status = '1'";
        $this->Query($q_show);*/
        return $this->fetchArray();
    }
    function langAll() {
        /*$q_show = "SELECT id,content FROM languages  WHERE status = '1'";
        $this->Query($q_show);*/
        return $this->fetchArray();
    }
    function siteLanguage() {

        return $arrData;
    }

    function rollback() {
        global $conn;
        echo mysqli_query($conn, "rollback");
    }
    function commit() {
        global $conn;
        echo mysqli_query($conn, "commit");
    }
    function defaultPageData() {
        $this->Query("SELECT * FROM confic WHERE title = 'paging'");
        $data = $this->fetchArray();
        return $data[0]['value']?$data[0]['value']:5;
    }


    /**************************************Start Here****************************/

    function moneyFormatIndia($nums) {
        $negative_value_check =  substr($nums,0,1);
        if($negative_value_check=='-'){
            $ve = '-';
            $num = substr($nums,1);
        }
        else{
            $ve = '';
            $num = $nums;
        }

        $explrestunits = "" ;
        if(strlen($num)>3) {
            $lastthree = substr($num, strlen($num)-3, strlen($num));
            $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
            $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for($i=0; $i<sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if($i==0) {
                    $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i].",";
                }
            }
            $thecash = $explrestunits.$lastthree;
        } else {
            $thecash = $num;
        }
        return $ve.$thecash; // writes the final format where $currency is the currency symbol.
    }


    function country_name($id) {
        $sql = "SELECT `country_name` FROM `country` WHERE `id`='".$id."'";
        $this->Query($sql);
        $country = $this->fetchArray();
        return $country[0]['country_name'];
    }


    function state_name($id) {
        $sql = "SELECT `state_name` FROM `state` WHERE `id`='".$id."'";
        $this->Query($sql);
        $state = $this->fetchArray();
        return $state[0]['state_name'];
    }

    function city_name($id) {
        $sql = "SELECT `city_name` FROM `city` WHERE `id`='".$id."'";
        $this->Query($sql);
        $city = $this->fetchArray();
        return $city[0]['city_name'];
    }

    function menu_name($id) {
        $sql = "SELECT `name` FROM `menus` WHERE `id`='".$id."'";
        $this->Query($sql);
        $city = $this->fetchArray();
        return $city[0]['name'];
    }


    function className($id) {
        $sql = "SELECT `name` FROM `class_master` WHERE `id`='".$id."'";
        $this->Query($sql);
        $city = $this->fetchArray();
        return $city[0]['name'];
    }


    function classDetails($id) {
        $ids = $id?" and id='".$id."'":'';
        $sql = "SELECT * FROM `class_master` WHERE 1 $ids";
        $this->Query($sql);
        return $this->fetchArray();
    }



    function feetypeName($id) {
        $sql = "SELECT `name` FROM `fee_type` WHERE `id`='".$id."'";
        $this->Query($sql);
        $city = $this->fetchArray();
        return $city[0]['name'];
    }

    function AdmissionNo() {
        $sql = "SELECT count(`id`) as `num` FROM `student_list` WHERE 1";
        $this->Query($sql);
        $city = $this->fetchArray();
        return $city[0]['num'];
    }

    function StudentName($id) {
        $sql = "SELECT * FROM `student_list` WHERE `id`='".$id."'";
        $this->Query($sql);
        $student = $this->fetchArray();
        return $student[0]['fname'].' '.$student[0]['mname'].' '.$student[0]['lname'];
    }

    function StudentDetails($id) {
        $ids = $id?" and (`id`='".$id."' || admission_no='".$id."')":'';
        $sql = "SELECT * FROM `student_list` WHERE 1 $ids";
        $this->Query($sql);
        return $this->fetchArray();

    }

    function totalFeeDetails($bill_no) {

        $sql = "SELECT * FROM `bill_fare` WHERE `bill_no`='".$bill_no."'";
        $this->Query($sql);
        return $this->fetchArray();

    }

    function admission_test_student($id) {
        $sql = "SELECT * FROM `admission_test` WHERE `id`='".$id."'";
        $this->Query($sql);
        $city = $this->fetchArray();
        return $city[0]['first_name'].' '.$city[0]['middle_name'].' '.$city[0]['last_name'];
    }


    /*=================Check Dulplicate ==================*/
    function checkDuplicate($table,$column,$key,$idcheck,$column1,$key1,$column2,$key2,$column3,$key3){

        $idcheck = ($idcheck!='')?" AND ".$idcheck:'';

        $col = ($column!='')?" AND `".$column."` LIKE '".$key."'":'';
        $col1 = ($column1!='')?" AND `".$column1."` LIKE '".$key1."'":'';
        $col2 = ($column2!='')?" AND `".$column2."` LIKE '".$key2."'":'';
        $col3 = ($column3!='')?" AND `".$column3."` LIKE '".$key3."'":'';

        $sql = "SELECT * FROM `".$table."` WHERE 1 $idcheck $col $col1 $col2 $col3";

        $this->Query($sql);
        $this->Execute();
        $count = $this->numRow();
        return $count;
    }

    function userName($id) {
        $sql = "SELECT `name` FROM users WHERE id='".$id."'";
        $this->Query($sql);
        $users = $this->fetchArray();
        return $users[0]['name'];
    }



    function log_report($activity) {
        $query =  "INSERT INTO `server_log`( `emp_id`, `system_ip`,`browser`, `activity`,  `date`) VALUES ('".$_SESSION['adminid']."', '".$_SESSION['sys_ip']."', '".$_SERVER['HTTP_USER_AGENT']."', '".$activity."','".date('Y-m-d H:i:s')."')";
        $this->Query($query);
        $this->Execute();
        //return $users[0]['name'];
    }

    function countLength($str) {
        $count_id_length = strlen($str);

        if($count_id_length==1){ $countLength    = '0000';	}
        if($count_id_length==2){ $countLength    = '000';	}
        if($count_id_length==3){ $countLength    = '00';  }
        if($count_id_length==4){ $countLength    = '0';   }
        return $countLength.$str;
    }



    /**************************************End Here****************************/



}
?>