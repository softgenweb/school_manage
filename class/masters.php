<?php

require_once('dbaccess.php');
require_once('textconfig/config.php');	

if(file_exists('configuration.php')){		
	require_once('configuration.php');
}

class MasterClass extends DbAccess {
		public $view='';
		public $name='master';		

	/*=============================Start Fee Type===================================*/
	function show_fees(){
		$query = "SELECT * FROM `fee_type` WHERE 1";
		$this->Query($query);
		$results = $this->fetchArray();
		
		$id = $_REQUEST['id'];
		if($id){
			$querySql = "SELECT * FROM `fee_type` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}				
		require_once("views/".$this->name."/".$this->task.".php"); 
	}

	function addnew_fees(){
		$id = $_REQUEST['id'];
		if($id){
			$query = "SELECT * FROM `fee_type` WHERE `id`='".$id."'";
			$this->Query($query);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}else{
			require_once("views/".$this->name."/".$this->task.".php"); 		
		}
	}

	function save_fees(){

		$name = trim($_REQUEST['name']);
		$id = $_REQUEST['id'];		
		 $idcheck = $id?'id!='.$id:'';
		$chk =  $this->checkDuplicate('fee_type','name',$name,$idcheck); 
	if($chk<1){
			if(!$id){
				$query = "INSERT INTO `fee_type`(`name`, `date_created`) VALUES ('".$name."', '".date('Y-m-d H:i:s')."')";
				$_SESSION['alertmessage'] = ADDNEWRECORD;
			}else{
	
				$query = "UPDATE `fee_type` SET `name`='".$name."' WHERE `id`='".$id."'";
				$_SESSION['alertmessage'] = UPDATERECORD;
			}
	
			$this->Query($query);
			$this->Execute();
	
			$_SESSION['errorclass'] = SUCCESSCLASS;
			}else{
				$_SESSION['alertmessage'] = DUPLICATE;
				$_SESSION['errorclass'] = ERRORCLASS;
			}
		header("location:index.php?control=master&task=show_fees");

	}

	function status_fees(){

		$id = $_REQUEST['id'];
		$status = $_REQUEST['status'];

		$query = "UPDATE `fee_type` SET `status`='".$status."' WHERE `id`='".$id."'";

		$this->Query($query);
		$this->Execute();

		$_SESSION['alertmessage'] = UPDATERECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=master&task=show_fees");
	}
	/*==============================End Fee Type========================================*/


	/*==================================Start Class Master========================*/
	function show_class(){

          $this->sqlQuery(class_master);
        /*  $query = "SELECT * FROM `class_master` WHERE 1";
		  $this->Query($query);*/
		  $results = $this->fetchArray();
		
		$id = $_REQUEST['id'];
		if($id){
			$querySql = "SELECT * FROM `class_master` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}

		require_once("views/".$this->name."/".$this->task.".php"); 

	}

	function addnew_class(){

		$id = $_REQUEST['id'];

		if($id){
			$query = "SELECT * FROM `class_master` WHERE `id`='".$id."'";
			$this->Query($query);
			$results = $this->fetchArray();

			require_once("views/".$this->name."/".$this->task.".php"); 
		}else{
			require_once("views/".$this->name."/".$this->task.".php"); 		
		}
	}

	function save_class(){

		$name = trim($_REQUEST['name']);
		$admission_fee = trim($_REQUEST['admission_fee']);
		$monthly_fee = trim($_REQUEST['monthly_fee']);
		$admission_kit = trim($_REQUEST['admission_kit']);
		$total = trim($_REQUEST['total']);
		$id = $_REQUEST['id'];
		$idcheck = $id?'id!='.$id:'';
		$chk =  $this->checkDuplicate('class_master','name',$name,$idcheck); 
		if($chk<1){
			if(!$id){
				$query = "INSERT INTO `class_master`(`name`, `admission_fee`, `monthly_fee`, `admission_kit`, `total`, `date_created`) VALUES ('".$name."', '".$admission_fee."', '".$monthly_fee."', '".$admission_kit."', '".$total."', '".date('Y-m-d H:i:s')."')";
				$_SESSION['alertmessage'] = ADDNEWRECORD;
			}else{

				$query = "UPDATE `class_master` SET `name`='".$name."', `admission_fee`='".$admission_fee."', `monthly_fee`='".$monthly_fee."', `admission_kit`='".$admission_kit."', `total`='".$total."' WHERE `id`='".$id."'";
				$_SESSION['alertmessage'] = UPDATERECORD;
			}

			$this->Query($query);
			$this->Execute();

			$_SESSION['errorclass'] = SUCCESSCLASS;
		}else{
			$_SESSION['alertmessage'] = DUPLICATE;
			$_SESSION['errorclass'] = ERRORCLASS;
		}
			header("location:index.php?control=master&task=show_class");

	}

	function status_class(){

		$id = $_REQUEST['id'];
		$status = $_REQUEST['status'];

		$query = "UPDATE `class_master` SET `status`='".$status."' WHERE `id`='".$id."'";

		$this->Query($query);
		$this->Execute();

		$_SESSION['alertmessage'] = UPDATERECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=master&task=show_class");
	}

	/*=============================End Class Master=========================================*/
	
	
	/*==================================Start Class Section Master========================*/
	function show_class_section(){
		$query = "SELECT * FROM `class_section` WHERE 1";
		$this->Query($query);
		$results = $this->fetchArray();
		
		$id = $_REQUEST['id'];
		if($id){
			$querySql = "SELECT * FROM `class_section` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}

		require_once("views/".$this->name."/".$this->task.".php"); 

	}


	function save_section(){

		$name = strtoupper($_REQUEST['name']);
		$id = $_REQUEST['id'];
        $idcheck = $id?'id!='.$id:'';
		$chk =  $this->checkDuplicate('class_section','name',$name,$idcheck); 
		if($chk<1){
		if(!$id){
			$query = "INSERT INTO `class_section`(`name`, `date`) VALUES ('".$name."', '".date('Y-m-d H:i:s')."')";
			$_SESSION['alertmessage'] = ADDNEWRECORD;
		}else{

			$query = "UPDATE `class_section` SET `name`='".$name."' WHERE `id`='".$id."'";
			$_SESSION['alertmessage'] = UPDATERECORD;
		}

		$this->Query($query);
		$this->Execute();		
		$_SESSION['errorclass'] = SUCCESSCLASS;
		}else{
			$_SESSION['alertmessage'] = DUPLICATE;
			$_SESSION['errorclass'] = ERRORCLASS;
		}

		header("location:index.php?control=master&task=show_class_section");

	}

	function status_section(){

		$id = $_REQUEST['id'];
		$status = $_REQUEST['status'];

		$query = "UPDATE `class_section` SET `status`='".$status."' WHERE `id`='".$id."'";

		$this->Query($query);
		$this->Execute();

		$_SESSION['alertmessage'] = UPDATERECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=master&task=show_class_section");
	}
	
	
	/*==================================End Class Section Master========================*/
	
	
		function pay_mode(){
			$query = "SELECT * FROM `payment_mode` WHERE 1 ORDER BY `mode` ASC";
			$this->Query($query);
			$results = $this->fetchArray();		
			
			$id = $_REQUEST['id'];
		   if($id){
			$querySql = "SELECT * FROM `payment_mode` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		    }

			require_once("views/".$this->name."/".$this->task.".php");			
		}

		function addnew_paymode(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM payment_mode WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save_paymode(){
			$mode = strtoupper($_REQUEST['mode']);		
			$id = $_REQUEST['id'];
			
			$idcheck = $id?'id!='.$id:'';
		   $chk =  $this->checkDuplicate('payment_mode','mode',$mode,$idcheck); 
            if($chk<1){
			if(!$id){
				$query = "INSERT INTO `payment_mode`(`mode`, `date_created`) VALUES ('".$mode."', '".date('Y-m-d H:i:s')."')";
				$_SESSION['alertmessage'] = ADDNEWRECORD; 


			/*===================Activity Log====================*/
			$activity = "Add Payment Mode (".$mode.") by ".$_SESSION['username'];


			}else{
				$query = "UPDATE `payment_mode` SET `mode`='".$mode."' WHERE `id`='".$id."'";
				$_SESSION['alertmessage'] = UPDATERECORD; 
			/*===================Activity Log====================*/			
			$activity = "Update by ".($this->userName($_SESSION['adminid']))." to Payment Mode changes : (".$mode.")"; 

			}
            $this->Query($query);
		    $this->Execute();
			
			$_SESSION['errorclass'] = SUCCESSCLASS;
			}else{
			$_SESSION['alertmessage'] = DUPLICATE;
			$_SESSION['errorclass'] = ERRORCLASS;
		    }
			
			
			 $this->log_report($activity);
			
			header("location:index.php?control=master&task=pay_mode");
		}

		function status_paymode(){
		
			$query="UPDATE `payment_mode` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['id']."'";	
			$this->Query($query);	
			$this->Execute();

			header("location:index.php?control=master&task=pay_mode");

		}
	/*============================End payment Mode==========================================*/
	
	
		/*=============================Start Student Pick-up Point ===================================*/
	function pickup_point(){
		$query = "SELECT * FROM `pickup_point` WHERE 1";
		$this->Query($query);
		$results = $this->fetchArray();
		
		$id = $_REQUEST['id'];
		if($id){
			$querySql = "SELECT * FROM `pickup_point` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}				
		require_once("views/".$this->name."/".$this->task.".php"); 
	}


	function save_pickup_point(){

		$name = strtoupper($_REQUEST['name']);
		$amount = $_REQUEST['amount'];
		$id = $_REQUEST['id'];	
		 $idcheck = $id?'id!='.$id:'';
		$chk =  $this->checkDuplicate('pickup_point','name',$name,$idcheck); 
	if($chk<1){
			if(!$id){
				$query = "INSERT INTO `pickup_point`(`name`, amount, `date_created`) VALUES ('".$name."', '".$amount."', '".date('Y-m-d H:i:s')."')";
				$_SESSION['alertmessage'] = ADDNEWRECORD;
			}else{	
				 $query = "UPDATE `pickup_point` SET `name`='".$name."', `amount`='".$amount."' WHERE `id`='".$id."'";				
				$_SESSION['alertmessage'] = UPDATERECORD;
			}
	
			$this->Query($query);
			$this->Execute();
	
			$_SESSION['errorclass'] = SUCCESSCLASS;
			}else{
				$_SESSION['alertmessage'] = DUPLICATE;
				$_SESSION['errorclass'] = ERRORCLASS;
			}
		header("location:index.php?control=master&task=pickup_point");

	}

	function status_pickup_point(){

		$id = $_REQUEST['id'];
		$status = $_REQUEST['status'];

		$query = "UPDATE `pickup_point` SET `status`='".$status."' WHERE `id`='".$id."'";

		$this->Query($query);
		$this->Execute();

		$_SESSION['alertmessage'] = UPDATERECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=master&task=pickup_point");
	}
	/*==============================End   Student Pick-up Point ========================================*/
	
	
	

	/*==================================Start Class Master========================*/
	function class_fee_master(){
		$class_master_id = $_REQUEST['class_master_id']?" and class_master_id='".$_REQUEST['class_master_id']."'":'';
		//$query = "SELECT * FROM `class_fee_master` WHERE 1 $class_master_id";
        $query = "SELECT * FROM `class_fee_master` WHERE 1 ";
		$this->Query($query);
		$results = $this->fetchArray();
		
		      $id = $_REQUEST['id'];
		
		
		if($id){
		    $querySql = "SELECT * FROM `class_fee_master` WHERE 1  and id='".$_REQUEST['id']."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}
        require_once("views/".$this->name."/".$this->task.".php"); 
      }


	function save_class_fee_master(){

		$class_master_id = trim($_REQUEST['class_master_id']);
		$fee_type_id = trim($_REQUEST['fee_type_id']);
		$fee_amount = trim($_REQUEST['fee_amount']);
		$day = trim($_REQUEST['day']);
		$fee_fine = trim($_REQUEST['fee_fine']);
		$id = $_REQUEST['id'];
		$idcheck = $id?'id!='.$id:'';
		$chk =  $this->checkDuplicate('class_fee_master','class_master_id',$class_master_id,$idcheck,'fee_type_id',$fee_type_id); 
		if($chk<1){
			if(!$id){
				$query = "INSERT INTO `class_fee_master`(`class_master_id`, `fee_type_id`, `fee_amount`, `day`, `fee_fine`, `date`) VALUES ('".$class_master_id."', '".$fee_type_id."', '".$fee_amount."', '".$day."', '".$fee_fine."', '".date('Y-m-d H:i:s')."')";
				$_SESSION['alertmessage'] = ADDNEWRECORD;
			}else{
	         /*===================Activity Log====================*/			
			$activity = "Update by ".($this->userName($_SESSION['adminid']))." to Class ".$class_master_id." fee Amount ".$fee_amount.""; 
			
				echo $query = "UPDATE `class_fee_master` SET `class_master_id`='".$class_master_id."', `fee_type_id`='".$fee_type_id."', `fee_amount`='".$fee_amount."', `day`='".$day."', `fee_fine`='".$fee_fine."' WHERE `id`='".$id."'";
				
				$_SESSION['alertmessage'] = UPDATERECORD;
				
			
			}

			$this->Query($query);
			$this->Execute();

			$_SESSION['errorclass'] = SUCCESSCLASS;
		}else{
			$_SESSION['alertmessage'] = DUPLICATE;
			$_SESSION['errorclass'] = ERRORCLASS;
		}
		 
		 $class_master_id = $class_master_id?'&class_master_id='.$class_master_id:'';
		
			//header("location:index.php?control=master&task=class_fee_master".$class_master_id);
        header("location:index.php?control=master&task=class_fee_master");
	}

	function status_class_fee_master(){

		$id = $_REQUEST['id'];
		$status = $_REQUEST['status'];

		$query = "UPDATE `class_fee_master` SET `status`='".$status."' WHERE `id`='".$id."'";

		$this->Query($query);
		$this->Execute();

		$_SESSION['alertmessage'] = UPDATERECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=master&task=class_fee_master");
	}

	/*=============================End Class Master=========================================*/
	
	
	
}
