<?php 	
	require_once('dbaccess.php');
	require_once('textconfig/config.php');		
	
	if(file_exists('configuration.php')){
		
		require_once('configuration.php');
	}
	
	class menuClass extends DbAccess {
		public $view='';
		public $name='menu';
		
		
		function show()
		{	
			$query = "SELECT * FROM `menus` WHERE 1";
			$this->Query($query);
			$results = $this->fetchArray();
			
			require_once("views/".$this->name."/".$this->task.".php");
		}		
		
		function addnew()
		{	
			$id = $_REQUEST['id'];
			if($id){
				$query = "SELECT * FROM `menus` WHERE `id`='".$id."'";
				$this->Query($query);
				$results = $this->fetchArray();
			}
			
			require_once("views/".$this->name."/".$this->task.".php");
		}
		
		function addnew_chlid()
		{	
			$id = $_REQUEST['id'];
			if($id){
				$query = "SELECT * FROM `child_menus` WHERE `id`='".$id."'";
				$this->Query($query);
				$results = $this->fetchArray();
			}
			
			require_once("views/".$this->name."/".$this->task.".php");
		}	
		
		function save()
		{	
			$name = $_REQUEST['name'];
			$parent = $_REQUEST['parent'];
			$icon = $_REQUEST['icon'];
			$url = $_REQUEST['url'];
			$control = $_REQUEST['control_new'];
			$task = $_REQUEST['task_new'];
			$id = $_REQUEST['id'];

			if(!$id){
				$query = "INSERT INTO `menus`(`parent`, `name`, `icon`, `url`, `control`, `task`, `date_created`, `date_modify`) VALUES ('".$parent."', '".$name."', '".$icon."', '".$url."', '".$control."', '".$task."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
				$this->Query($query);
				$this->Execute();
				$last_id = mysqli_insert_id();

				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				if($parent=='1'){
					header('location:index.php?control=menu&task=addnew_chlid&pid='.$last_id);
				}else{
					header('location:index.php?control=menu&task=show');
				}

			}else{

				$query = "UPDATE `menus` SET `parent`='".$parent."',`name`='".$name."',`icon`='".$icon."',`url`='".$url."',`control`='".$control."',`task`='".$task."',`date_modify`='".date('Y-m-d H:i:s')."' WHERE `id`='".$id."'";
				$this->Query($query);
				$this->Execute();
				// $last_id = mysqli_insert_id();

				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				if($parent=='1'){
					header('location:index.php?control=menu&task=addnew_chlid&pid='.$id);
				}else{
					header('location:index.php?control=menu&task=show');
				}				
			}
			
			
		}	
		
		function child_save()
		{	
			$name = $_REQUEST['name'];
			$parent = $_REQUEST['parent'];
			$icon = $_REQUEST['icon'];
			$url = $_REQUEST['url'];
			$control = $_REQUEST['control_new'];
			$task = $_REQUEST['task_new'];
			$id = $_REQUEST['id'];

			if(!$id){
				$query = "INSERT INTO `child_menus`(`parent`, `name`, `icon`, `url`, `control`, `task`, `date_created`, `date_modify`) VALUES ('".$parent."', '".$name."', '".$icon."', '".$url."', '".$control."', '".$task."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
				$this->Query($query);
				$this->Execute();
				$last_id = mysqli_insert_id();

				$_SESSION['alertmessage'] = ADDNEWRECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				
			}else{

				$query = "UPDATE `child_menus` SET `parent`='".$parent."',`name`='".$name."',`icon`='".$icon."',`url`='".$url."',`control`='".$control."',`task`='".$task."',`date_modify`='".date('Y-m-d H:i:s')."' WHERE `id`='".$id."'";
				$this->Query($query);
				$this->Execute();
				// $last_id = mysqli_insert_id();

				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
								
			}
			
			header('location:index.php?control=menu&task=show');
		}
	
	
	}
