<?php

require_once('dbaccess.php');
require_once('textconfig/config.php');	

if(file_exists('configuration.php')){		
	require_once('configuration.php');
}

class StudentClass extends DbAccess {
		public $view='';
		public $name='student';		

	/*=============================Student  Master===================================*/
	function show(){
		$query = "SELECT * FROM `student_list` WHERE 1";
		$this->Query($query);
		$results = $this->fetchArray();
		
		$id = $_REQUEST['id'];
		if($id){
			$querySql = "SELECT * FROM `student_list` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}

		require_once("views/".$this->name."/".$this->task.".php"); 

	}


	function show_detail(){

		$admission_no = $_REQUEST['admission_no'];

		if($admission_no){
			$query = "SELECT * FROM `student_list` WHERE `admission_no`='".$admission_no."'";
			$this->Query($query);
			$results = $this->fetchArray();

			require_once("views/".$this->name."/".$this->task.".php"); 
		}else{
			require_once("views/".$this->name."/".$this->task.".php"); 		
		}
	}

	function save(){

		$class_id = trim($_REQUEST['class_id']);	$fname = trim($_REQUEST['fname']);
		$mname = trim($_REQUEST['mname']);			$lname = trim($_REQUEST['lname']);
		$gender = trim($_REQUEST['gender']);		$dob = trim($_REQUEST['dob']);
		$blood_group = trim($_REQUEST['blood_group']);	$religion = trim($_REQUEST['religion']);
		$caste = trim($_REQUEST['caste']);			$nationality = trim($_REQUEST['nationality']);
		$aadhar_no = trim($_REQUEST['aadhar_no']);	$community = trim($_REQUEST['community']);
		$known_language = trim($_REQUEST['known_language']);	
		$mother_tongue = trim($_REQUEST['mother_tongue']);
		$residential_address = mysqli_real_escape_string($_REQUEST['residential_address']);
		$parent_mobile = trim($_REQUEST['parent_mobile']);
		$father_name = trim($_REQUEST['father_name']);	
		$mother_name = trim($_REQUEST['mother_name']);
		$correspondence_address = mysqli_real_escape_string($_REQUEST['correspondence_address']);
		$guardian_mobile = trim($_REQUEST['guardian_mobile']);
		$guardian_name = trim($_REQUEST['guardian_name']);
		$session = $_SESSION['financial_year'];	
		$admission_test_id = $_REQUEST['admission_test_id'];
		$section_id = $_REQUEST['section_id'];
		$transport = ($_REQUEST['transport']==1)?$_REQUEST['transport']:2;
		$pickup_point_id = $_REQUEST['pickup_point_id'];
         
		  if($class_id){
           $class_data =  mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `class_master` WHERE `id`='".$class_id."'"));
		   $admission_fee = $class_data['admission_fee'];
		   $monthly_fee = $class_data['monthly_fee'];
		   $admission_kit = $class_data['admission_kit'];
		 }


         if($pickup_point_id){
           $fare_charge =  mysqli_fetch_array(mysqli_query($conn,"SELECT amount FROM `pickup_point` WHERE `id`='".$pickup_point_id."'"));
		   $fare_amount = $fare_charge['amount'];
		 }
		  
		  $grand_total = $admission_fee+$monthly_fee+$admission_kit+$fare_amount;
			

		$id = $_REQUEST['id'];
		 $idcheck = $id?'id!='.$id:'';
		// $chk =  $this->checkDuplicate('fee_type','name',$name); 
	// if($chk<1){
		
	
			
				/*==========Check Duplicate Value (Upto 3 Columns)=========*/
				$chk =  $this->checkDuplicate('student_list','class_id',$class_id, $idcheck, 'fname',$fname,'dob',$dob,'father_name',$father_name); 
				if($chk<1){
					
				if(!$id){
				$query = "INSERT INTO `student_list`(`class_id`, `section_id`, `fname`, `mname`, `lname`, `gender`, `dob`, `blood_group`, `religion`, `caste`, `nationality`, `aadhar_no`, `community`, `known_language`, `mother_tongue`, `residential_address`, `parent_mobile`, `father_name`, `mother_name`, `correspondence_address`, `guardian_mobile`, `guardian_name`, transport, pickup_point_id, fare_amount, admission_fee, monthly_fee, admission_kit,  grand_total, `session`, `date_created`, `date_modify`) VALUES ('".$class_id."', '".$section_id."', '".$fname."', '".$mname."', '".$lname."', '".$gender."', '".$dob."', '".$blood_group."', '".$religion."', '".$caste."', '".$nationality."', '".$aadhar_no."', '".$community."', '".$known_language."', '".$mother_tongue."', '".$residential_address."', '".$parent_mobile."', '".$father_name."', '".$mother_name."', '".$correspondence_address."', '".$guardian_mobile."', '".$guardian_name."', '".$transport."', '".$pickup_point_id."', '".$fare_amount."', '".$admission_fee."', '".$monthly_fee."', '".$admission_kit."', '".$grand_total."', '".$session."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
			
				$this->Query($query);
				$this->Execute();
				$last_id = mysqli_insert_id();

				$sn = $this->AdmissionNo();

				$sl_no = (strlen($sn) ==1)?("0000".$sn):
				((strlen($sn) ==2)?("000".$sn):
				((strlen($sn) ==3)?("00".$sn):
				((strlen($sn) ==4)?("0".$sn):($sn))));

				$admission_id = $_SESSION['fyear'].$sl_no;
				
				if (!is_dir('media/student/' . $last_id)) {
							mkdir('media/student/' . $last_id, 0777, true);
				}
				$student_photo = $_FILES['student_photo'];
			    $img_photo= $last_id."_photo";
				$folder= "media/student/$last_id/";				
				 $temp = explode(".", $student_photo);
	             $filename = $img_photo.'.' . end($temp);				
				move_uploaded_file($student_photo["tmp_name"] , $folder.$filename);				
				$student_photo_path = $folder.$filename;

				$this->Query("UPDATE `student_list` SET `admission_no`='".$admission_id."', student_photo='".$student_photo_path."' WHERE `id`='".$last_id."'");
				$this->Execute();
				
			
				$this->Query("UPDATE `admission_test` SET `exam_result`='3' WHERE `id`='".$admission_test_id."'");
				$this->Execute();

				$_SESSION['alertmessage'] = ADDNEWRECORD;
				$_SESSION['errorclass'] = SUCCESSCLASS;
				header("location:index.php?control=student&task=show");

				}
                 else{
				
				$image = '';
			     if ($_FILES['student_photo']['name'] != "") {
				if (!is_dir('media/student/' . $id)) {
							mkdir('media/student/' . $id, 0777, true);
				}
				$student_photo = $_FILES['student_photo'];
			    $img_photo= $id."_photo";
				$folder= "media/student/$id/";				
				 $temp = explode(".", $student_photo['name']);
	             $filename = $img_photo.'.png'; //. end($temp);				
				move_uploaded_file($student_photo["tmp_name"] , $folder.$filename);				
				$student_photo_path = $folder.$filename;
				
				$image = ", student_photo='".$student_photo_path."'";
			}

			 $query = "UPDATE `student_list` SET `class_id`='".$class_id."', `section_id`='".$section_id."', `fname`='".$fname."',`mname`='".$mname."',`lname`='".$lname."',`gender`='".$gender."',`dob`='".$dob."',`blood_group`='".$blood_group."',`religion`='".$religion."',`caste`='".$caste."',`nationality`='".$nationality."',`aadhar_no`='".$aadhar_no."',`community`='".$community."',`known_language`='".$known_language."',`mother_tongue`='".$mother_tongue."',`residential_address`='".$residential_address."',`parent_mobile`='".$parent_mobile."',`father_name`='".$father_name."',`mother_name`='".$mother_name."',`correspondence_address`='".$correspondence_address."',`guardian_mobile`='".$guardian_mobile."',`guardian_name`='".$guardian_name."', `transport`='".$transport."', `pickup_point_id`='".$pickup_point_id."', `fare_amount`='".$fare_amount."', `admission_fee`='".$admission_fee."', `monthly_fee`='".$monthly_fee."', `admission_kit`='".$admission_kit."', `grand_total`='".$grand_total."',   `date_modify`='".date('Y-m-d H:i:s')."' $image WHERE `id`='".$id."'";
	

			$this->Query($query);
			$this->Execute();
			$_SESSION['alertmessage'] = UPDATERECORD;
			$_SESSION['errorclass'] = SUCCESSCLASS;
			header("location:index.php?control=student&task=show");
			}
               }
			   else{
				$_SESSION['alertmessage'] = DUPLICATE;
				$_SESSION['errorclass'] = ERRORCLASS;	
				header("location:index.php?control=student&task=show");
				}
	}

	function status(){

		$id = $_REQUEST['id'];
		$status = $_REQUEST['status'];

		$query = "UPDATE `student_list` SET `status`='".$status."' WHERE `id`='".$id."'";

		$this->Query($query);
		$this->Execute();

		$_SESSION['alertmessage'] = UPDATERECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=student&task=show");
	}
	/*==============================End Student details========================================*/

	/*==============================Admission Test========================*/
	function show_admission_exam(){
		$query = "SELECT * FROM `admission_test` WHERE 1";
		$this->Query($query);
		$results = $this->fetchArray();
		
		
		$id = $_REQUEST['id'];
		if($id){
			$querySql = "SELECT * FROM `admission_test` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}

		require_once("views/".$this->name."/".$this->task.".php"); 

	}
	
	
	function addnew_admission_exam(){
		
			$id = $_REQUEST['id'];
		if($id){
			$querySql = "SELECT * FROM `admission_test` WHERE `id`='".$id."'";
			$this->Query($querySql);
			$datas = $this->fetchArray();
		}

		require_once("views/".$this->name."/".$this->task.".php"); 
		
	}
	


	function save_admission_exam(){

		$class_id = $_REQUEST['class_id'];
		$first_name = strtoupper($_REQUEST['first_name']);
		$middle_name = strtoupper($_REQUEST['middle_name']);
		$last_name = strtoupper($_REQUEST['last_name']);
		$father_name = strtoupper($_REQUEST['father_name']);
		$dob = $_REQUEST['dob'];
		$gender = $_REQUEST['gender'];
		$parents_mobile = $_REQUEST['parents_mobile'];
		$exam_fee = $_REQUEST['exam_fee'];
		// $student_name = $_REQUEST['student_name'];
		$id = $_REQUEST['id'];

		if(!$id){
			$query = "INSERT INTO `admission_test`(`class_id`, `first_name`, `middle_name`, `last_name`, `father_name`, `dob`, `gender`, `parents_mobile`, `exam_fee`, `date_created`, `date_modify`) VALUES ('".$class_id."', '".$first_name."', '".$middle_name."', '".$last_name."', '".$father_name."', '".$dob."', '".$gender."', '".$parents_mobile."', '".$exam_fee."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";

			$_SESSION['alertmessage'] = ADDNEWRECORD;
		}else{

			$query = "UPDATE `admission_test` SET `class_id`='".$class_id."', `first_name`='".$first_name."', `middle_name`='".$middle_name."', `last_name`='".$last_name."', `father_name`='".$father_name."', `dob`='".$dob."', `gender`='".$gender."', `parents_mobile`='".$parents_mobile."', `exam_fee`='".$exam_fee."', `date_modify`='".date('Y-m-d H:i:s')."' WHERE `id`='".$id."'";

			$_SESSION['alertmessage'] = UPDATERECORD;
		}

		$this->Query($query);
		$this->Execute();

		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=student&task=show_admission_exam");

	}

	function status_admission_exam(){ //for result

		$id = $_REQUEST['id'];
		$status = $_REQUEST['exam_result'];

		$query = "UPDATE `admission_test` SET `exam_result`='".$status."' WHERE `id`='".$id."'";

		$this->Query($query);
		$this->Execute();

		$_SESSION['alertmessage'] = UPDATERECORD;
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=student&task=show_admission_exam");
	}


/*	function new_admission(){
		$reg_id = $_REQUEST['rege_id'];
		if($reg_id){
			$query = "SELECT * FROM `admission_test` WHERE `id`='".$reg_id."'";
			$this->Query($query);
			$results = $this->fetchArray();
			header("location:index.php?control=student&task=addnew");
		}
	}*/
	/*======================================================================*/
	
	function admission_fee(){

		$admission_no = $_REQUEST['admission_no'];
		    $bill_no  = $_REQUEST['bill_no']?" and bill_no='".$_REQUEST['bill_no']."'":'';

		if($admission_no){
			//$query = "SELECT * FROM `student_list` WHERE `admission_no`='".$admission_no."'";
			$query = "SELECT * FROM `student_fee` WHERE `admission_no`='".$admission_no."' $bill_no";
			$this->Query($query);
			$results = $this->fetchArray();
			
			
			$uquery = "SELECT * FROM `bill_fare` WHERE `admission_no`='".$admission_no."'";
			$this->Query($uquery);		
		    $uresults = $this->fetchArray();
		    $tdata=count($uresults);

		/* Paging start here */
			$page   = intval($_REQUEST['page']);
			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
			$adjacents  = intval($_REQUEST['adjacents']);
			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 
			$tdata = floor($tdata);
			if($page<=0)  $page  = 1;
			if($adjacents<=0) $tdata?($adjacents = 4):0;
			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	
		/* Paging end here */	

		$query = $uquery .' LIMIT  '.(($page-1)*$tpages).",".$tpages;
		$this->Query($query);
		$datas = $this->fetchArray();	
       /*   echo '<pre>';			
		   print_r($datas);*/
	
		
		require_once("views/".$this->name."/admission_fee.php"); 
		}else{
			require_once("views/".$this->name."/".$this->task.".php"); 		
		}
	}
	
	
	
		function admission_fee_save(){
            global $conn;
	     $student_list_id = $_REQUEST['student_list_id'];
            $bill_fare_id = $_REQUEST['id'];
			    $class_id = $_REQUEST['class_id'];
			$admission_no = $_REQUEST['admission_no'];

			/*=========Array============*/
			$fee_type_id = $_REQUEST['fee_type_id'];
			$fee_amount = $_REQUEST['fee_amount'];
			$discount = $_REQUEST['discount'];
			$amount_after_discount = $_REQUEST['amount_after_discount'];
			$paid_amount = $_REQUEST['paid_amount'];
			/*=========================*/

			$total_fee_amount = $_REQUEST['total_fee_amount'];
			$total_discount = $_REQUEST['total_discount'];
			$total_discount_amount = $_REQUEST['total_discount_amount'];
			$total_amount_after_discount = $_REQUEST['total_amount_after_discount'];
			$total_paid_amount = $_REQUEST['total_paid_amount'];
			$total_remaining_amount = $_REQUEST['total_remaining_amount'];
		
			$select_paymode = $_REQUEST['select_paymode'];
			$total_item = $_REQUEST['total_item'];
			$financial_year = $_SESSION['fyear'];

            $fyear = date('y').(date('y') + 1);
			
			$bill_no = $_REQUEST['bill_no'];
			$bill_date = $_REQUEST['bill_date'];



			if($bill_fare_id==''){



			$query = mysqli_query($conn,"INSERT INTO `bill_fare`(`admission_no`, `student_list_id`, `total_item`, `total_fee_amount`, `total_discount`, total_discount_amount, `total_amount_after_discount`, `total_paid_amount`, total_remaining_amount, `financial_year`, `payment_mode`, `date_created`, `date_modify`, `emp_id`,bill_no) VALUES ('".$admission_no."','".$student_list_id."','".$total_item."','".$total_fee_amount."','".$total_discount."','".$total_discount_amount."','".$total_amount_after_discount."','".$total_paid_amount."','".$total_remaining_amount."','".$financial_year."','".$select_paymode."', '".date('Y-m-d')."', '".date('Y-m-d H:i:s')."', '".$_SESSION['adminid']."','".$bill_no."' )");

                $last_id = mysqli_insert_id($conn);

                $totalBill = mysqli_fetch_array(mysqli_query($conn,"SELECT count(id)+1 as id  FROM `bill_fare` WHERE `financial_year`='".$financial_year."'"));

                $bill_no = $fyear.rand('1000','9999').$this->countLength($totalBill['id']);

                $queryBillNo = "UPDATE `bill_fare` SET `bill_no`='".$bill_no."' WHERE `id`='".$last_id."'";
                $this->Query($queryBillNo);
                $this->Execute();
			
			if($last_id>0){
			for($i=0; $i<$total_item ; $i++) { 
			
			$discount_amount = round((($fee_amount[$i] * $discount[$i])/100));			
			$sqlFine = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `class_fee_master` WHERE `fee_type_id`='".$fee_type_id[$i]."' and class_master_id='".$class_id."'"));
			$remaining_amount =  $amount_after_discount[$i]-$paid_amount[$i];			
			$fine_amount = ($remaining_amount!='0')?$sqlFine['fee_fine']:'0';
			$status = ($remaining_amount!='0')?'2':'1';
			
			$class_fee_master_id = $sqlFine['id']; /// Class id & fee Type id both create a class_fee_master table id uses here///////////
			
			
						
		    $sql = "INSERT INTO `student_fee`(`student_list_id`, `admission_no`, `class_id`, `fee_type_id`, class_fee_master_id, `name`, `fee_amount`, `discount`, discount_amount, `amount_after_discount`, `paid_amount`, `remaining_amount`, `fine_amount`, `session`, `date`, `time`, `status`, `bill_fare_id`, `bill_no`) VALUES ('".$student_list_id."', '".$admission_no."', '".$class_id."', '".$fee_type_id[$i]."', '".$class_fee_master_id."', '".$this->feetypeName($fee_type_id[$i])."', '".$fee_amount[$i]."', '".$discount[$i]."', '".$discount_amount."', '".$amount_after_discount[$i]."', '".$paid_amount[$i]."', '".$remaining_amount."', '".$fine_amount."', '".$financial_year."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '".$status."', '".$last_id."', '".$bill_no."')";
		
               mysqli_query($conn, $sql);
			   
			   
			}

			/*===================Activity Log====================*/
			$activity = "Add New Bill (".$bill_no.") of total amount:".$grand_total;

			$add = mysqli_query($conn,"INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
			/*===================================================*/ 
			$_SESSION['alertmessage'] = ADDNEWRECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;	
			$_SESSION['bno'] = $bill_no;	
			// $_SESSION['print_bill'] = '1';


			
			/*echo "<script>window.open('index.php?control=billing&task=print_bill&bno=".$bill_no."','_blank')</script>";
			echo "<script>window.open('index.php?control=billing&task=addnew','_self')</script>";*/
			//header("location:index.php?control=billing&task=print_bill&bno=$bill_no");
			// header("location:index.php?control=billing&task=addnew");
			 header("location:index.php?control=student&task=admission_fee&admission_no=".$admission_no);
		
		     } 


		  }

			else{


                $query = mysqli_query($conn,"UPDATE `bill_fare` set `total_item`='".$total_item."', `total_fee_amount`='".$total_fee_amount."', `total_discount`='".$total_discount."', total_discount_amount='".$total_discount_amount."', `total_amount_after_discount`='".$total_amount_after_discount."', `total_paid_amount`='".$total_paid_amount."', total_remaining_amount='".$total_remaining_amount."', `financial_year`='".$financial_year."', `payment_mode`='".$select_paymode."', `date_modify`= '".date('Y-m-d H:i:s')."', `emp_id`='".$_SESSION['adminid']."' where id='".$bill_fare_id."' and bill_no='".$bill_no."'");

                $delete = mysqli_query($conn,"DELETE from student_fee where bill_fare_id='".$bill_fare_id."' and bill_no='".$bill_no."'");

                    for($i=0; $i<$total_item; $i++) {

                        $discount_amount = round((($fee_amount[$i] * $discount[$i])/100));
                        $sqlFine = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `class_fee_master` WHERE `fee_type_id`='".$fee_type_id[$i]."' and class_master_id='".$class_id."'"));
                        $remaining_amount =  $amount_after_discount[$i]-$paid_amount[$i];
                        $fine_amount = ($remaining_amount!='0')?$sqlFine['fee_fine']:'0';
                        $status = ($remaining_amount!='0')?'2':'1';

                        $class_fee_master_id = $sqlFine['id']; /// Class id & fee Type id both create a class_fee_master table id uses here///////////



                        $sql = "INSERT INTO `student_fee`(`student_list_id`, `admission_no`, `class_id`, `fee_type_id`, class_fee_master_id, `name`, `fee_amount`, `discount`, discount_amount, `amount_after_discount`, `paid_amount`, `remaining_amount`, `fine_amount`, `session`, `date`, `time`, `status`, `bill_fare_id`, `bill_no`) VALUES ('".$student_list_id."', '".$admission_no."', '".$class_id."', '".$fee_type_id[$i]."', '".$class_fee_master_id."', '".$this->feetypeName($fee_type_id[$i])."', '".$fee_amount[$i]."', '".$discount[$i]."', '".$discount_amount."', '".$amount_after_discount[$i]."', '".$paid_amount[$i]."', '".$remaining_amount."', '".$fine_amount."', '".$financial_year."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '".$status."', '".$bill_fare_id."', '".$bill_no."')";

                        mysqli_query($conn, $sql);


                    }

                    /*===================Activity Log====================*/
                    $activity = "Add New Bill (".$bill_no.") of total amount:".$grand_total;

                    $add = mysqli_query($conn,"INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
                    /*===================================================*/
                    $_SESSION['alertmessage'] = ADDNEWRECORD;
                    $_SESSION['errorclass'] = SUCCESSCLASS;
                    $_SESSION['bno'] = $bill_no;

                header("location:index.php?control=student&task=admission_fee&admission_no=".$admission_no);

                }
	  
		}
	
	
	
	
	function fee_details(){

		$id = $_REQUEST['id'];

		if($id){
			$query = "SELECT * FROM `student_list` WHERE `id`='".$id."'";
			$this->Query($query);
			$results = $this->fetchArray();

			require_once("views/".$this->name."/".$this->task.".php"); 
		}else{
			require_once("views/".$this->name."/".$this->task.".php"); 		
		}
	}

}
