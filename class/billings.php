<?php require_once('dbaccess.php');
require_once('textconfig/config.php');	

if(file_exists('configuration.php')){		
	require_once('configuration.php');
}

class BillingClass extends DbAccess {
		public $view='';
		public $name='billing';

	/*=============================Fee Master===================================*/
	function show(){
		$query = "SELECT * FROM `fee_type` WHERE 1";
		$this->Query($query);
		$results = $this->fetchArray();

		require_once("views/".$this->name."/".$this->task.".php"); 

	}

/*	function addnew(){

		$id = $_REQUEST['id'];

		if($id){
			$query = "SELECT * FROM `fee_type` WHERE `id`='".$id."'";
			$this->Query($query);
			$results = $this->fetchArray();

			require_once("views/".$this->name."/".$this->task.".php");
		}else{
			require_once("views/".$this->name."/".$this->task.".php");
		}
	}*/


    function addnew(){

        $admission_no = $_REQUEST['admission_no']?" and admission_no='".$_REQUEST['admission_no']."'":'';
        $bill_no  = $_REQUEST['bill_no']?" and bill_no='".$_REQUEST['bill_no']."'":'';

        if($admission_no) {
            //$query = "SELECT * FROM `student_list` WHERE `admission_no`='".$admission_no."'";
            $query = "SELECT * FROM `student_fee` WHERE 1 $admission_no $bill_no";
            $this->Query($query);
            $results = $this->fetchArray();
        }

            $uquery = "SELECT * FROM `bill_fare` WHERE  date_created='".date('Y-m-d')."'";
            $this->Query($uquery);
            $uresults = $this->fetchArray();
            $tdata=count($uresults);

            /* Paging start here */
            $page   = intval($_REQUEST['page']);
            $_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default
            $adjacents  = intval($_REQUEST['adjacents']);
            $tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];//
            $tdata = floor($tdata);
            if($page<=0)  $page  = 1;
            if($adjacents<=0) $tdata?($adjacents = 4):0;
            $reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;
            /* Paging end here */

            $query = $uquery .' LIMIT  '.(($page-1)*$tpages).",".$tpages;
            $this->Query($query);
            $datas = $this->fetchArray();
               /*echo '<pre>';
                print_r($datas);*/


            require_once("views/".$this->name."/".$this->task.".php");

    }


    function fee_save(){
        global $conn;
        $student_list_id = $_REQUEST['student_list_id'];
        $bill_fare_id = $_REQUEST['id'];
        $class_id = $_REQUEST['class_id'];
        $admission_no = $_REQUEST['admission_no'];

        /*=========Array============*/
        $fee_type_id = $_REQUEST['fee_type_id'];
        $fee_amount = $_REQUEST['fee_amount'];
        $discount = $_REQUEST['discount'];
        $amount_after_discount = $_REQUEST['amount_after_discount'];
        $paid_amount = $_REQUEST['paid_amount'];
        /*=========================*/

        $total_fee_amount = $_REQUEST['total_fee_amount'];
        $total_discount = $_REQUEST['total_discount'];
        $total_discount_amount = $_REQUEST['total_discount_amount'];
        $total_amount_after_discount = $_REQUEST['total_amount_after_discount'];
        $total_paid_amount = $_REQUEST['total_paid_amount'];
        $total_remaining_amount = $_REQUEST['total_remaining_amount'];

        $select_paymode = $_REQUEST['select_paymode'];
        $total_item = $_REQUEST['total_item'];
        $financial_year = $_SESSION['fyear'];

        $fyear = date('y').(date('y') + 1);

        $bill_no = $_REQUEST['bill_no'];
        $bill_date = $_REQUEST['bill_date'];



        if($bill_fare_id==''){


            $query = mysqli_query($conn, "INSERT INTO `bill_fare`(`admission_no`, `student_list_id`, `total_item`, `total_fee_amount`, `total_discount`, total_discount_amount, `total_amount_after_discount`, `total_paid_amount`, total_remaining_amount, `financial_year`, `payment_mode`, `date_created`, `date_modify`, `emp_id`,bill_no) VALUES ('".$admission_no."','".$student_list_id."','".$total_item."','".$total_fee_amount."','".$total_discount."','".$total_discount_amount."','".$total_amount_after_discount."','".$total_paid_amount."','".$total_remaining_amount."','".$financial_year."','".$select_paymode."', '".date('Y-m-d')."', '".date('Y-m-d H:i:s')."', '".$_SESSION['adminid']."','".$bill_no."' )");

           $last_id = mysqli_insert_id($conn);

            $totalBill = mysqli_fetch_array(mysqli_query($conn, "SELECT count(id)+1 as id  FROM `bill_fare` WHERE `financial_year`='".$financial_year."'"));
            $bill_no = $fyear.rand('1000','9999').$this->countLength($totalBill['id']);

            $queryBillNo = "UPDATE `bill_fare` SET `bill_no`='".$bill_no."' WHERE `id`='".$last_id."'";
            $this->Query($queryBillNo);
            $this->Execute();


            if($last_id>0){
                for($i=0; $i<$total_item ; $i++) {

                    $discount_amount = round((($fee_amount[$i] * $discount[$i])/100));
                    $sqlFine = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `class_fee_master` WHERE `fee_type_id`='".$fee_type_id[$i]."' and class_master_id='".$class_id."'"));
                    $remaining_amount =  $amount_after_discount[$i]-$paid_amount[$i];
                    $fine_amount = ($remaining_amount!='0')?$sqlFine['fee_fine']:'0';
                    $status = ($remaining_amount!='0')?'2':'1';

                    $class_fee_master_id = $sqlFine['id']; /// Class id & fee Type id both create a class_fee_master table id uses here///////////



                    $sql = "INSERT INTO `student_fee`(`student_list_id`, `admission_no`, `class_id`, `fee_type_id`, class_fee_master_id, `name`, `fee_amount`, `discount`, discount_amount, `amount_after_discount`, `paid_amount`, `remaining_amount`, `fine_amount`, `session`, `date`, `time`, `status`, `bill_fare_id`, `bill_no`) VALUES ('".$student_list_id."', '".$admission_no."', '".$class_id."', '".$fee_type_id[$i]."', '".$class_fee_master_id."', '".$this->feetypeName($fee_type_id[$i])."', '".$fee_amount[$i]."', '".$discount[$i]."', '".$discount_amount."', '".$amount_after_discount[$i]."', '".$paid_amount[$i]."', '".$remaining_amount."', '".$fine_amount."', '".$financial_year."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '".$status."', '".$last_id."', '".$bill_no."')";

                    mysqli_query($conn,$sql);


                }

                /*===================Activity Log====================*/
                $activity = "Add New Bill (".$bill_no.") of total amount:".$grand_total;

                $add = mysqli_query($conn,"INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
                /*===================================================*/
                $_SESSION['alertmessage'] = ADDNEWRECORD;
                $_SESSION['errorclass'] = SUCCESSCLASS;
                $_SESSION['bno'] = $bill_no;
                // $_SESSION['print_bill'] = '1';



                /*echo "<script>window.open('index.php?control=billing&task=print_bill&bno=".$bill_no."','_blank')</script>";
                echo "<script>window.open('index.php?control=billing&task=addnew','_self')</script>";*/
                //header("location:index.php?control=billing&task=print_bill&bno=$bill_no");
                // header("location:index.php?control=billing&task=addnew");
                header("location:index.php?control=billing&task=addnew");

            }


        }

        else{


            $query = mysqli_query($conn, "UPDATE `bill_fare` set `total_item`='".$total_item."', `total_fee_amount`='".$total_fee_amount."', `total_discount`='".$total_discount."', total_discount_amount='".$total_discount_amount."', `total_amount_after_discount`='".$total_amount_after_discount."', `total_paid_amount`='".$total_paid_amount."', total_remaining_amount='".$total_remaining_amount."', `financial_year`='".$financial_year."', `payment_mode`='".$select_paymode."', `date_modify`= '".date('Y-m-d H:i:s')."', `emp_id`='".$_SESSION['adminid']."' where id='".$bill_fare_id."' and bill_no='".$bill_no."'");

            $delete = mysqli_query($conn, "DELETE from student_fee where bill_fare_id='".$bill_fare_id."' and bill_no='".$bill_no."'");

            for($i=0; $i<$total_item; $i++) {

                $discount_amount = round((($fee_amount[$i] * $discount[$i])/100));
                $sqlFine = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `class_fee_master` WHERE `fee_type_id`='".$fee_type_id[$i]."' and class_master_id='".$class_id."'"));
                $remaining_amount =  $amount_after_discount[$i]-$paid_amount[$i];
                $fine_amount = ($remaining_amount!='0')?$sqlFine['fee_fine']:'0';
                $status = ($remaining_amount!='0')?'2':'1';

                $class_fee_master_id = $sqlFine['id']; /// Class id & fee Type id both create a class_fee_master table id uses here///////////



                $sql = "INSERT INTO `student_fee`(`student_list_id`, `admission_no`, `class_id`, `fee_type_id`, class_fee_master_id, `name`, `fee_amount`, `discount`, discount_amount, `amount_after_discount`, `paid_amount`, `remaining_amount`, `fine_amount`, `session`, `date`, `time`, `status`, `bill_fare_id`, `bill_no`) VALUES ('".$student_list_id."', '".$admission_no."', '".$class_id."', '".$fee_type_id[$i]."', '".$class_fee_master_id."', '".$this->feetypeName($fee_type_id[$i])."', '".$fee_amount[$i]."', '".$discount[$i]."', '".$discount_amount."', '".$amount_after_discount[$i]."', '".$paid_amount[$i]."', '".$remaining_amount."', '".$fine_amount."', '".$financial_year."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '".$status."', '".$bill_fare_id."', '".$bill_no."')";

                mysqli_query($conn, $sql);


            }

            /*===================Activity Log====================*/
            $activity = "Add New Bill (".$bill_no.") of total amount:".$grand_total;

            $add = mysqli_query($conn,"INSERT INTO `activity_log`(`system_ip`, `activity`, `user_id`, `date_created`) VALUES ('".$_SESSION['sys_ip']."', '".$activity."', '".$_SESSION['adminid']."', '".date('Y-m-d H:i:s')."')");
            /*===================================================*/
            $_SESSION['alertmessage'] = ADDNEWRECORD;
            $_SESSION['errorclass'] = SUCCESSCLASS;
            $_SESSION['bno'] = $bill_no;

            header("location:index.php?control=billing&task=addnew");

        }

    }


	

}
