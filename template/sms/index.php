<?php
   header("Pragma: no-cache");
   header("Cache-Control: no-cache");
   header("Expires: 0");
   
   ?>
<!DOCTYPE html>
<html>
   <head>
      <base href="/school_manage/" >
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>School Fee Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $tmp;?>/style.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/select2/select2.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"> -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo $tmp;?>/plugins/select2/select2.full.min.js"></script>
   </head>
   <style type="text/css">
    .select2-container .select2-selection--single{
      height: 34px !important;
      text-align: left;
    }
    iframe.skiptranslate, .goog-logo-link, #goog-gt-tt{
      display: none !important;
      height: 0px !important;
    }
    body{
      top:0px !important;
    }
    .goog-te-gadget{
      color: #fff !important;
    }
    .goog-te-combo {
      display: block;
      width: 100%;
      height: 34px;
      padding: 6px 12px;
      font-size: 14px;
      line-height: 1.42857143;
      color: #555;
      background-color: #fff;
      background-image: none;
      border: 1px solid #ccc;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
      box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
      -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
      -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
      transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
	
	
	@media only screen and (max-width: 600px) {
  .header_box {
    display:none !important;
  } 
  .main-sidebar {
 top: 0px !important;
}
}

@media only screen and (max-width: 767px) {
 .main-sidebar {
 top: 0px !important;/*109px !important;*/
}
}
	
	
	
    </style>
   <body class="hold-transition skin-blue sidebar-mini">
    
      <div class="wrapper">
         <div class="main-header hidden-lg hidden-md hidden-sm "> <a href="#" class="hidden-lg hidden-md hidden-sm sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            </a>    <span style="color: #fff;float: left;padding: 15px 0 0 60px;">School Fee Management</span>    
            <a href="logout.php" class="logout hidden-lg hidden-md hidden-sm" style="color: #fff;float: right;padding: 15px;">Sign out</a> &nbsp; &nbsp;
    </div>

 </div>

<?php //include('includes/header.php'); ?>
      <div class="wrapper">
         <?php include_once('includes/sidebar.php');?>
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">        
          <?php include_once('includes/navigation.php');?>

            <section class="content">
               <?php include_once('controller.php'); ?>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
        
         <div class="control-sidebar-bg"></div>
      </div>
 
 
 
 <?php //include_once('includes/footer.php');
 
 /***********************Footer Section added in Include->footer.php after complete work Below Code Cut & paste***************/
 ?>
 
 
  <!--   <footer class="main-footer">
        <div class="pull-right hidden-xs">
         
        </div>
         Copyright &copy; 2017 All rights reserved.<strong> <a href="http://softgentechnologies.com/" target="_blank">SOFTGEN TECHNOLOGIES PVT. LTD.</a></strong> 
      </footer>-->

      <!-- Control Sidebar -->
      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
  
   <!-- jQuery 2.1.4 -->

    <!-- <script src="<?php echo $tmp;?>/plugins/jQuery/jQuery-2.1.4.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $tmp;?>/bootstrap/js/bootstrap.min.js"></script>
     <!-- DataTables -->
    <script src="<?php echo $tmp;?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $tmp;?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
    <!-- <script src="<?php echo $tmp;?>/plugins/morris/morris.min.js"></script> -->
    <!-- Sparkline -->
    <!-- <script src="<?php echo $tmp;?>/plugins/sparkline/jquery.sparkline.min.js"></script> -->
    <!-- jvectormap -->
    <!-- <script src="<?php echo $tmp;?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> -->
    <!-- <script src="<?php echo $tmp;?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> -->
    <!-- jQuery Knob Chart -->
    <!-- <script src="<?php echo $tmp;?>/plugins/knob/jquery.knob.js"></script> -->
    <!-- daterangepicker -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> -->
    <script src="<?php echo $tmp;?>/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo $tmp;?>/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo $tmp;?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo $tmp;?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $tmp;?>/plugins/fastclick/fastclick.min.js"></script> 
       <!-- AdminLTE App -->
    <script src="<?php echo $tmp;?>/dist/js/app.min.js"></script>
   
      <script>
        $('.datepicker').datepicker({
          format:'dd/mm/yyyy',
          autoclose: true,
          endDate: '-10m'
        });
      $(function () {
        $("#example1-1").DataTable({
           "iDisplayLength": 25
        });
        $('#example1-2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

	  $(function () {
        $("#example1-3").DataTable();
        $('#example1-4').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });

   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });

$( ".sortable" ).sortable();
$( ".sortable" ).disableSelection();

$('a.logout').click(function () {
    document.cookie ="googtrans=/en/en;  path=/";
});

$(document).ready(function(){
  $('.goog-te-combo').addClass('select2');

});
  $('.select2').select2();
</script>
  </body>
</html>
