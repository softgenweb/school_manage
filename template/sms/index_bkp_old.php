<?php include('includes/header.php'); ?>
      <div class="wrapper">
         <?php include_once('includes/sidebar.php');?>
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">        
          <?php include_once('includes/navigation.php');?>

            <section class="content">
               <?php include_once('controller.php'); ?>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
        
         <div class="control-sidebar-bg"></div>
      </div>
 <?php include_once('includes/footer.php');?>